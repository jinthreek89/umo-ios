//
//  String+UMO.swift
//  UMO
//
//  Created by Boris on 7/3/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import Foundation

extension String {
    var length: Int {
        return characters.count
    }
    
    var shortUserName: String {
        let explode = characters.split(" ").map(String.init)
        if explode.count > 1 {
            let firstWord: String = explode[0]
            let secondWord: String = explode[1]
            
            let range1 = Range(firstWord.startIndex ..< firstWord.startIndex.advancedBy(1))
            let range2 = Range(secondWord.startIndex ..< secondWord.startIndex.advancedBy(1))
            
            let firstCharacter = firstWord[range1]
            let secondCharacter = secondWord[range2]
            
            return firstCharacter.uppercaseString + secondCharacter.uppercaseString
            
        } else {
            if self.length > 1 {
                let range = Range(self.startIndex ..< self.startIndex.advancedBy(2))
                let twoCharacter = self[range]
                return twoCharacter.uppercaseString
            } else {
                return self.uppercaseString
            }
        }
    }
}