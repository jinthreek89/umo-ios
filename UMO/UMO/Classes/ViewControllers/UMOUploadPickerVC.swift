//
//  UMOUploadPickerVC.swift
//  UMO
//
//  Created by Boris on 7/8/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import BRYXBanner

class UMOUploadPickerVC: UMOUploadBaseVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var imgviewBlurBackground: UIImageView!
    @IBOutlet weak var imgviewSelectedPhoto: UIImageView!
    @IBOutlet weak var clsviewLibrary: UICollectionView!
    
    @IBOutlet weak var btnNext: UIButton!
    
    var resourceType = UPLOADRESOURCE.NONE
    var selectedFile: AnyObject?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupLayout() {
        super.setupLayout()
        
        makeRoundView(self.btnNext)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.imgviewBlurBackground.bounds
        
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] // for supporting device rotation
        self.imgviewBlurBackground.addSubview(blurEffectView)

        addGestureForFullScreenImage(self.imgviewSelectedPhoto)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

    @IBAction func onPressCancel(sender: AnyObject) -> Void {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onPressNext(sender: AnyObject) {
        if self.selectedFile == nil {
            
            self.showNotificationAlert(title: NSLocalizedString("No file", comment: ""), message: NSLocalizedString("Please select file to upload.", comment: ""))
            
        } else {
            self.performSegueWithIdentifier(SEGUE.GOTOFinalPostVC, sender: self)
        }
    }
 
    
    
    // MARK: - UICollectionView datasource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: UMOFileCell = collectionView.dequeueReusableCellWithReuseIdentifier(UMOFileCell.identifier, forIndexPath: indexPath) as! UMOFileCell
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = (collectionView.frame.size.width - 42) / 4
        return CGSize(width: width, height: width)
    }
    // MARK: delegate

}
