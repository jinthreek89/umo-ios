//
//  UMOCreditsVC.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit


class UMOCreditsVC: UMOViewController {

    static let identifier: String = "UMOCreditsVC"
    
    @IBOutlet weak var lblAvailableCredits: UILabel!
    @IBOutlet weak var btnCredits1: UIButton!
    @IBOutlet weak var btnCredits5: UIButton!
    @IBOutlet weak var btnCredits20: UIButton!
    @IBOutlet weak var btnCreditsOther: UIButton!
    
    override func setupLayout() {
        super.setupLayout()
        
//        makeRoundView(self.btnCredits1?.superview)
        let radius = self.btnCredits1.frame.size.width * 0.5
        makeRoundView(self.btnCredits1, radius: radius)
        makeRoundView(self.btnCredits5, radius: radius)
        makeRoundView(self.btnCredits20, radius: radius)
        makeRoundView(self.btnCreditsOther, radius: radius)
        self.btnCreditsOther.layer.borderColor = self.btnCreditsOther.titleLabel?.textColor.CGColor
        self.btnCreditsOther.layer.borderWidth = 1
    }
    
    @IBAction func onPressCancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) { 
            
        }
    }
}
