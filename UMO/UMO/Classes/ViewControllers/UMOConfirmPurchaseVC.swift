//
//  UMOConfirmPurchaseVC.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOConfirmPurchaseVC: UMOTableViewController {
    
    @IBOutlet weak var btnConfirmPurchase: UIButton!
    
    override func setupLayout() {
        super.setupLayout()
        
        makeRoundView(self.btnConfirmPurchase)
    }
}
