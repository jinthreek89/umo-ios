//
//  UMOFavoriteVC.swift
//  UMO
//
//  Created by Boris on 7/4/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOFavoriteVC: UMOMainBaseCollectionVC {

    static let cellIdentifier = "ImageCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 60
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(UMOFavoriteVC.cellIdentifier, forIndexPath: indexPath)
        
        return cell
    }
    
}
