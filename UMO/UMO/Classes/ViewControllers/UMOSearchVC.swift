//
//  UMOSearchVC.swift
//  UMO
//
//  Created by Boris on 7/4/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit


class UMOSearchVC: UMOMainBaseVC, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    static let ImageCellIdentifier   = "ImageCell"
    static let SearchResultCellIdentifier = "SearchResultCell"
    
    @IBOutlet weak var clsviewImages: UICollectionView!
    @IBOutlet weak var tblSearchResult: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var constrainBottopSpaceOfTableView: NSLayoutConstraint!
    
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func setupLayout() {
        super.setupLayout()
//        self.collectionView?.
        
        self.tblSearchResult.hidden = true
        self.clsviewImages.contentInset = UIEdgeInsets.init(top: 5, left: 5, bottom: 0, right: 5)
        
        self.searchBar.tintColor = UIColor.whiteColor()
        if #available(iOS 9.0, *) {
            UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).tintColor = UIColor.grayColor()
        } else {

        }
        
//        UIBarButtonItem.appearanceForTraitCollection(UISearchBar(), whenContainedInInstancesOfClasses: nil)
        
    }
    
    override func keyboardWillShowRect(keyboardSize: CGSize) {
        constrainBottopSpaceOfTableView.constant = keyboardSize.height - 49
        updateConstraintWithAnimate(false)
    }
    
    override func keyboardWillHideRect() {
        constrainBottopSpaceOfTableView.constant = 0
    }
    
    // MARK: - UICollectionView datasource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 60
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(UMOSearchVC.ImageCellIdentifier, forIndexPath: indexPath)
        
        return cell
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 3 - 8
        return CGSizeMake(width, width)
    }

    // MARK: - UITableView datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UMOSearchResultCell = tableView.dequeueReusableCellWithIdentifier(UMOSearchResultCell.Identifier) as! UMOSearchResultCell
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // MARK: UISearchBar delegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.length > 0 {
            self.tblSearchResult.hidden = false
            self.tblSearchResult.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        self.searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.searchBar.text = nil
        self.searchBar.setShowsCancelButton(false, animated: true)
        self.tblSearchResult.hidden = true
        searchBar.resignFirstResponder()
    }
    
}
