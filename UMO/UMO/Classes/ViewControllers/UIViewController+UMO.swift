//
//  UIViewController+UMO.swift
//  UMO
//
//  Created by Boris on 6/29/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController
import Firebase
import BRYXBanner


extension UIViewController {
    
    func makeRoundView(v : UIView?) -> Void {
        makeRoundView(v, radius: 5.0)
    }
    
    func makeRoundView(v : UIView?, radius r : CGFloat) -> Void {
        v?.layer.cornerRadius = r
        v?.clipsToBounds = true
    }
    
    func setupLayout() -> Void {
        
    }
    
    func addLeftPadding(padding : CGFloat, textField textfield : UITextField!) -> Void {
        let viewLeftPaddingF : UIView = UIView(frame: CGRect.init(x: 0, y: 0, width: padding, height: (textfield.frame.size.height)))
        textfield.leftViewMode = .Always
        textfield.leftView = viewLeftPaddingF
    }
    
    func keyboardWillShowRect(keyboardSize : CGSize) -> Void {
        
    }
    
    func keyboardWillHideRect() -> Void {
        
    }
    
    func updateConstraintWithAnimate(animate: Bool) -> Void {
        if animate == true {
            self.view.setNeedsUpdateConstraints()
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseOut, animations: {
                self.view.layoutIfNeeded()
                }, completion: { (complete) in
                    
            })
        } else {
            updateViewConstraints()
        }
    }
    
    func appManager() -> UMOAppManager {
        return UMOAppManager.sharedManager
    }
    
    func databaseRef() -> FIRDatabaseReference {
        return FIRDatabase.database().reference()
    }
    
    func storageRef() -> FIRStorageReference {
        return FIRStorage.storage().referenceForURL(FIREBASE.STORAGE)
    }
    
    func addCustomNavigationBar() -> Void {
        let btnAttribute = [NSFontAttributeName: UIFont.init(name: APPFONT.REGULAR, size: 12)!,
                            NSForegroundColorAttributeName: APPCOLOR.DARK_BLUE]
        let btnUpload: UIBarButtonItem = UIBarButtonItem.init(title: NSLocalizedString("Upload", comment: ""), style: .Plain, target: self, action: #selector(UIViewController.onPressNavigationItemUpload(_:)))
        btnUpload.setTitleTextAttributes(btnAttribute, forState: .Normal)
        navigationItem.leftBarButtonItem = btnUpload
        
        let btnCredit: UIBarButtonItem = UIBarButtonItem.init(title: NSLocalizedString("Credits", comment: ""), style: .Plain, target: self, action: #selector(UIViewController.onPressNavigationItemCredits(_:)))
        btnCredit.setTitleTextAttributes(btnAttribute, forState: .Normal)
        navigationItem.rightBarButtonItem = btnCredit
        
        let appLogo: UIImageView = UIImageView.init(image: UIImage.init(named: "iconNavLogo"))
        navigationItem.titleView = appLogo
    }
    
    func onPressNavigationItemUpload(sender: UIBarButtonItem) -> Void {
        print("upload pressed")
        
        let uploadNC: UMOUploadNC = self.storyboard!.instantiateViewControllerWithIdentifier(UMOUploadNC.identifier) as! UMOUploadNC
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: uploadNC)
        formSheetController.presentationController?.contentViewSize = CGSizeMake(300, 300)  // or pass in UILayoutFittingCompressedSize to size automatically with auto-layout
        formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = true
        formSheetController.presentationController?.blurEffectStyle = UIBlurEffectStyle.Dark
        formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
        formSheetController.contentViewControllerTransitionStyle = .SlideAndBounceFromLeft
        
        self.presentViewController(formSheetController, animated: true, completion: nil)

    }
    
    func onPressNavigationItemCredits(sender: UIBarButtonItem) -> Void {
        print("upload credits")
        
        let creditsVC: UMOCreditsVC = self.storyboard!.instantiateViewControllerWithIdentifier(UMOCreditsVC.identifier) as! UMOCreditsVC
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: creditsVC)
        formSheetController.presentationController?.contentViewSize = CGSizeMake(260, 260)  // or pass in UILayoutFittingCompressedSize to size automatically with auto-layout
        formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = true
        formSheetController.presentationController?.blurEffectStyle = UIBlurEffectStyle.Dark
        formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
        formSheetController.contentViewControllerTransitionStyle = .SlideAndBounceFromRight
        
        self.presentViewController(formSheetController, animated: true, completion: nil)
    }
    
    func addGestureForFullScreenImage(imageview: UIImageView!) -> Void {
        let tapPicture = UITapGestureRecognizer(target: self, action: #selector(UMOUploadBaseVC.fullscreenImage))
        imageview.addGestureRecognizer(tapPicture)
        imageview.userInteractionEnabled = true
    }
    
    func fullscreenImage(sender: UITapGestureRecognizer) -> Void {
        let imageView = sender.view as! UIImageView
        if imageView.image != nil {
            let viewController = SFFullscreenImageDetailViewController(imageView: imageView)
            viewController.presentInCurrentKeyWindow()
        }
    }
    
    func showNotificationAlert(title title: String?, message: String?) -> Void {
        let banner = Banner(title: title, subtitle: message, image: nil, backgroundColor: UIColor.redColor())
        banner.dismissesOnTap = true
        banner.show(duration: 3.0)

    }
    
    // MARK: - NSNotification Selector
    
    func notificationPostedMedia(sender: AnyObject) -> Void {
        
    }

}
