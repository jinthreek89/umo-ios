//
//  UMOTutorialVC.swift
//  UMO
//
//  Created by Boris on 6/30/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOTutorialVC: UMOLoginBaseVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var pageIndicator: UIPageControl!
    
    private let _arrayWelcomeImages: [String] = ["imageWelcome0", "imageWelcome0", "imageWelcome0", "imageWelcome0", "imageWelcome0"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setupLayout() {
        super.setupLayout()
        makeRoundView(self.btnSkip)
    }
    
    @IBAction func onSkip(sender: AnyObject) {
        navigationController?.performSegueWithIdentifier(SEGUE.GOTOMainTabVC, sender: self)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    // MARK: - UICollectionView datasource 
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _arrayWelcomeImages.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("WelcomeCell", forIndexPath: indexPath)
        let imgview: UIImageView = cell.contentView.viewWithTag(10) as! UIImageView
        imgview.image = UIImage.init(named: _arrayWelcomeImages[indexPath.row])
        return cell
    }
    
    // MARK: delegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let index: Int = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageIndicator.currentPage = index
    }
    
    // MARK: layout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let cellSize: CGSize = CGSize.init(width: collectionView.frame.size.width, height: collectionView.frame.size.height * 0.5)
        return cellSize
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
