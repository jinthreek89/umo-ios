//
//  UMOOutOfCreditsVC.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOOutOfCreditsVC: UMOViewController {

    static let identifier: String = "UMOOutOfCreditsVC"

    @IBOutlet weak var lblCurrentCredits: UILabel!
    @IBOutlet weak var btnBuyOneCredit: UIButton!
    @IBOutlet weak var btnBuy5Credits: UIButton!
    @IBOutlet weak var btnBuy20Credits: UIButton!
    @IBOutlet weak var btnBuy40Credits: UIButton!
    @IBOutlet weak var btnBuyOtherCredits: UIButton!

    override func setupLayout() {
        super.setupLayout()
        makeRoundView(self.btnBuyOneCredit)
        
        makeCircleBorder(self.btnBuy5Credits)
        makeCircleBorder(self.btnBuy20Credits)
        makeCircleBorder(self.btnBuy40Credits)
        makeCircleBorder(self.btnBuyOtherCredits)
        
    }
    
    func makeCircleBorder(button: UIButton) -> Void {
        let radius = button.frame.size.width * 0.5
        makeRoundView(button, radius: radius)
        button.layer.borderColor = button.titleLabel?.textColor.CGColor
        button.layer.borderWidth = 1
    }
    
    
    @IBAction func onPressCancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
