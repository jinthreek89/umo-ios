//
//  UMOUploadPickerNC.swift
//  UMO
//
//  Created by Boris on 7/8/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

class UMOUploadPickerNC: UMOUploadNC {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if appManager().uploadType == UPLOADTYPE.IMAGE {
            let photoPickerVC = self.storyboard?.instantiateViewControllerWithIdentifier(UMOUploadPhotoPickerVC.identifier) as! UMOUploadPhotoPickerVC
            photoPickerVC.resourceType = appManager().uploadResource
            self.viewControllers = [photoPickerVC]
        }
    }
}
