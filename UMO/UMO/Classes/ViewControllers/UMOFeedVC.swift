//
//  UMOFeedVC.swift
//  UMO
//
//  Created by Boris on 7/3/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase
import HMSegmentedControl
import CollectionViewWaterfallLayout

class UMOFeedVC: UMOMainBaseVC, UICollectionViewDelegate, UICollectionViewDataSource, CollectionViewWaterfallLayoutDelegate, UITableViewDataSource, UITableViewDelegate, UMOFeedMainCellDelegate {
    
//    var segmentControll: HMSegmentedControl = HMSegmentedControl.init(sectionTitles: [NSLocalizedString("My Feed", comment: ""), NSLocalizedString("Following", comment: "")])
    
    @IBOutlet weak var segmentControl: HMSegmentedControl!
    @IBOutlet weak var scrollViewMainContainer: UIScrollView!
    
    @IBOutlet weak var clsviewMyFeed: UICollectionView!
    @IBOutlet weak var tblFollowing: UITableView!
    
    lazy var cellSizes: [CGSize] = {
        var _cellSizes = [CGSize]()
        
        for _ in 0...100 {
            let random = Int(arc4random_uniform((UInt32(100))))
            
            _cellSizes.append(CGSize(width: Int(UIScreen.mainScreen().bounds.size.width * 0.5 - 5), height: 120 + random))
        }
        
        return _cellSizes
    }()
    
    lazy var cellSize: CGSize = {
        var _screenSize = UIScreen.mainScreen().bounds.size
        let _cellSize = CGSize(width: _screenSize.width / 2 - 5, height: _screenSize.width * 0.6)
        return _cellSize
    }()
    
    private var feedResult: [String: UMOPostModel] = [:]
    private var feedUIDs: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
        
        addEventForFeed()
        
    }
    
    override func setupLayout() {
        super.setupLayout()
        segmentControl.sectionTitles = [NSLocalizedString("My Feed", comment: ""), NSLocalizedString("Following", comment: "")]
        let segmentSelectedAttribute = [NSFontAttributeName: UIFont.init(name: APPFONT.REGULAR, size: 14)!,
                                        NSForegroundColorAttributeName: APPCOLOR.LIGHT_BLUE]
        let segmentNormalAttribute   = [NSFontAttributeName: UIFont.init(name: APPFONT.REGULAR, size: 14)!,
                                        NSForegroundColorAttributeName: APPCOLOR.GRAY_BLUE]
        
        self.segmentControl.indexChangeBlock = { (index: Int) -> Void in
            self.scrollViewMainContainer.setContentOffset(CGPointMake(self.scrollViewMainContainer.frame.size.width * CGFloat(index), 0), animated: true)
        }

//        segmentControl.borderType = .Bottom
//        segmentControl.borderColor = APPCOLOR.LIGHT_BLUE
//        segmentControl.borderWidth = 2
        segmentControl.selectionIndicatorColor      = APPCOLOR.LIGHT_BLUE
        segmentControl.selectionIndicatorHeight     = 2
        segmentControl.selectedTitleTextAttributes  = segmentSelectedAttribute
        segmentControl.titleTextAttributes          = segmentNormalAttribute
        segmentControl.selectionIndicatorLocation   = HMSegmentedControlSelectionIndicatorLocationDown
        
        
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//        layout.headerInset = UIEdgeInsetsMake(20, 0, 0, 0)
//        layout.headerHeight = 50
//        layout.footerHeight = 20
        layout.minimumColumnSpacing = 3
        layout.minimumInteritemSpacing = 2
        
        clsviewMyFeed.collectionViewLayout = layout
//        clsviewMyFeed.registerClass(UICollectionReusableView.self, forSupplementaryViewOfKind: CollectionViewWaterfallElementKindSectionHeader, withReuseIdentifier: "Header")
//        clsviewMyFeed.registerClass(UICollectionReusableView.self, forSupplementaryViewOfKind: CollectionViewWaterfallElementKindSectionFooter, withReuseIdentifier: "Footer")
        
        
    }
    
    func addEventForFeed() -> Void {
        
//        let myInfo = FIRAuth.auth()?.currentUser
        let childPath = "\(TABLENAME.MEDIA)"
        
        /*
        databaseRef().child(childPath).queryOrderedByKey().observeSingleEventOfType(.Value) { (snapshot: FIRDataSnapshot) in
            print(" #######  F E E D  ########### \n\(snapshot)")
        } */
        
        databaseRef().child(childPath).queryOrderedByKey().observeEventType(.ChildAdded) { (snapshot: FIRDataSnapshot) in
            print(" #######  Added Feed  ########### \n\(snapshot)")
            if snapshot.childrenCount > 0 {
                self.feedUIDs.insert(snapshot.key, atIndex: 0)
                self.feedResult[snapshot.key] = UMOPostModel(snapshot: snapshot)
                self.clsviewMyFeed.reloadData()
            }
        }
        
        databaseRef().child(childPath).queryOrderedByKey().observeEventType(.ChildChanged) { (snapshot: FIRDataSnapshot) in
            print(" #######  Changed Feed  ########### \n\(snapshot)")
            if snapshot.childrenCount > 0 {
//                self.feedUIDs.insert(snapshot.key, atIndex: 0)
                if self.feedUIDs.contains(snapshot.key) {
                    self.feedResult[snapshot.key] = UMOPostModel(snapshot: snapshot)
                    self.clsviewMyFeed.reloadData()
                }
            }
        }
        
        databaseRef().child(childPath).queryOrderedByKey().observeEventType(.ChildRemoved) { (snapshot: FIRDataSnapshot) in
            print(" #######  Removed Feed  ########### \n\(snapshot)")
            
        }
        
        /*
        databaseRef().child(childPath).queryOrderedByValue().queryStartingAtValue(myInfo?.uid, childKey: FIELDNAME.USER_UID).queryEndingAtValue(myInfo?.uid, childKey: FIELDNAME.USER_UID).observeEventType(.Value) { (snapshot: FIRDataSnapshot) in
            print(" #######  F E E D  ########### \n\(snapshot)")
        } */
                
//        databaseRef().child(TABLENAME.MEDIA).queryOrderedByKey().observeEventType(.Value) { (snapshot: FIRDataSnapshot) in
//            print(" #######  F E E D  ########### \n\(snapshot)")
//        }
        
    }
    
    
    // MARK: - UICollectionView datasource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedResult.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: UMOFeedMainCell = collectionView.dequeueReusableCellWithReuseIdentifier(UMOFeedMainCell.identifier, forIndexPath: indexPath) as! UMOFeedMainCell
        cell.delegate = self
        let keyName = feedUIDs[indexPath.row]
        let feed = feedResult[keyName]
        cell.setFeed(feed)
        return cell
    }

    /*
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        var reusableView: UICollectionReusableView? = nil
        
        if kind == CollectionViewWaterfallElementKindSectionHeader {
            reusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath)
            
            if let view = reusableView {
                view.backgroundColor = UIColor.redColor()
            }
        }
        else if kind == CollectionViewWaterfallElementKindSectionFooter {
            reusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Footer", forIndexPath: indexPath)
            if let view = reusableView {
                view.backgroundColor = UIColor.blueColor()
            }
        }
        
        return reusableView!
    }
 */
    
    // MARK: UMOFeedMainCell delegate
    
    func UMOFeedMainCellTapImage(cell: UMOFeedMainCell) {
        performSegueWithIdentifier(SEGUE.GOTOPurchaseDetailVC, sender: cell)
    }
    
    func UMOFeedMainCellFavorite(cell: UMOFeedMainCell) {
        print("clicked Favorite")
        
    }
    
    func UMOFeedMainCellPurchase(cell: UMOFeedMainCell) {
        print("clicked Purchase")
        performSegueWithIdentifier(SEGUE.GOTOPurchaseDetailVC, sender: cell)
    }
    
    // MARK: WaterfallLayoutDelegate
    
    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return cellSize
//        return cellSizes[indexPath.item]
    }

    // MARK: delegate
    
    // MARK: -
    
    // MARK: UITableView datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UMOMainFollowerCell = tableView.dequeueReusableCellWithIdentifier("UMOMainFollowerCell") as! UMOMainFollowerCell

        return cell
    }
    
    // MARK: delegate
    
    // MARL: UIScrollView delegate
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView == scrollViewMainContainer {
            let selectedTab = Int(scrollViewMainContainer.contentOffset.x / scrollViewMainContainer.frame.size.width)
            self.segmentControl.selectedSegmentIndex = selectedTab
        }
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SEGUE.GOTOPurchaseDetailVC {
            let cell = sender as! UMOFeedMainCell
            let purchaseVC = segue.destinationViewController as! UMOPurchaseDetailVC
            purchaseVC.setFeed(cell.getFeed())
        }
    }
}
