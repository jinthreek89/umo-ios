//
//  UMOViewController.swift
//  UMO
//
//  Created by Boris on 6/29/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit


class UMOViewController: UIViewController, NVActivityIndicatorViewable {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        addKeyboardNotification()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardNotification()
    }
    
    private func addKeyboardNotification() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UMOViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UMOViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
    }
    
    private func removeKeyboardNotification() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) -> Void {
        var keyboardBound : CGRect = CGRectZero
        notification.userInfo![UIKeyboardFrameEndUserInfoKey]?.getValue(&keyboardBound)
        keyboardWillShowRect(keyboardBound.size)
    }
    
    func keyboardWillHide(notification: NSNotification) -> Void {
        keyboardWillHideRect()
    }

    @IBAction func onBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onCancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onCancleNavigationController(sender: AnyObject) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func showLoadingIndicator(string: String?) -> Void {
        let size:CGSize = CGSize.init(width: 30, height: 30)
        startActivityAnimating(size, message: string, type: .BallRotate)
    }
    
    func hideLoadingIndicator() -> Void {
        stopActivityAnimating()
    }

    func hideLoadingIndicatorWithSimpleAlert(title: String?, Message message: String?, CloseButton closebutton: String?, Completion completion:(() -> Void)?) -> Void {
        let alertView = UIAlertController.init(title: title, message: message, preferredStyle: .Alert)
        let closeAction = UIAlertAction.init(title: closebutton, style: .Cancel) { (alertAction: UIAlertAction) in
            if completion != nil {
                completion!()
            }
        }
        alertView.addAction(closeAction)
        presentViewController(alertView, animated: true) { 
            self.hideLoadingIndicator()
        }
    }
    
    func showAlert(title: String?, Message message: String?, CloseButton closeButton: String?, Completion completion:(() -> Void)?) -> Void {
        hideLoadingIndicatorWithSimpleAlert(title, Message: message, CloseButton: closeButton, Completion: completion)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
