//
//  UMOUploadPhotoPickerVC.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Photos

class UMOUploadPhotoPickerVC: UMOUploadPickerVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    static let identifier = "UMOUploadPhotoPickerVC"
    
    var photos: [Int: UIImage?] = [:]
    var photosOfLocalGallery: PHFetchResult?
    var selectedImageIndex = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            if resourceType == UPLOADRESOURCE.LOCAL {
                fetchPhotoFromLocalGallery()
            }


//        fetchPhotoAtIndexFromEnd(0)
    }
    
    override func setupLayout() {
        super.setupLayout()
        
        var placeholderImageName: String!
        switch appManager().uploadType {
        case UPLOADTYPE.IMAGE:
            placeholderImageName = "placeholderImage"
            break
        case UPLOADTYPE.VIDEO:
            placeholderImageName = "placeholderVideo"
            break
        case UPLOADTYPE.AUDIO:
            placeholderImageName = "placeholderAudio"
            break
        case UPLOADTYPE.PDF:
            placeholderImageName = "placeholderPDF"
            break
        default:
            placeholderImageName = "placeholderImage"
            break
        }
        let placeholder = UIImage(named: placeholderImageName)
        
        self.imgviewSelectedPhoto.image = placeholder
        self.imgviewBlurBackground.image = placeholder
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    private func fetchPhotoFromLocalGallery() -> Void {
        
        // Note that if the request is not set to synchronous
        // the requestImageForAsset will return both the image
        // and thumbnail; by setting synchronous to true it
        // will return just the thumbnail
        let requestOptions = PHImageRequestOptions()
        requestOptions.synchronous = true
        
        // Sort the images by creation date
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: true)]
        
        photosOfLocalGallery = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: fetchOptions)

        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            print("This is run on the background queue")
            self.loadImageFromLocalGallery()
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print("This is run on the main queue, after the previous code in outer block")
                self.clsviewLibrary.reloadData()
            })
        })

    }
    
    private func loadImageFromLocalGallery() -> Void {
        let imageManager = PHImageManager.defaultManager()
        let requestOptions = PHImageRequestOptions()
        requestOptions.synchronous = true

        for index in 0 ..< (photosOfLocalGallery?.count)! {
            let item = photosOfLocalGallery?.objectAtIndex(index) as! PHAsset
            imageManager.requestImageForAsset(item, targetSize: CGSize(width: 60, height: 60), contentMode: .AspectFill, options: requestOptions, resultHandler: { (image: UIImage?, info: [NSObject : AnyObject]?) in
                if image != nil {
                    self.photos[index] = image!
                } else {
                    self.photos[index] = nil
                }
            })
        }
    }
    
    
    
    // MARK: - UICollectionView datasource
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if resourceType == UPLOADRESOURCE.LOCAL {
            return (photosOfLocalGallery?.count)! + 1
        } else {
            return 0
        }
        
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: UMOFileCell = collectionView.dequeueReusableCellWithReuseIdentifier(UMOFileCell.identifier, forIndexPath: indexPath) as! UMOFileCell
        let imageView = cell.imageView
        if resourceType == UPLOADRESOURCE.LOCAL {
            if indexPath.row == 0 {
                imageView.image = UIImage(named: "iconCamera")
                cell.viewCheckMark.hidden = true
            } else {
                if indexPath.row < (self.photos.count + 1){
                    imageView.image = self.photos[indexPath.row - 1]!
                    if indexPath.row - 1 == selectedImageIndex {
                        cell.viewCheckMark.hidden = false
                    } else {
                        cell.viewCheckMark.hidden = true
                    }
                }
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if resourceType == UPLOADRESOURCE.LOCAL {
            if indexPath.row == 0 {
                self.openCamera()
            } else {
                self.loadImageOfIndex(indexPath.row - 1)
                selectedImageIndex = indexPath.row - 1
                collectionView.reloadData()
            }
        }
    }
    
    private func loadImageOfIndex(index: Int) -> Void {
        let imageManager = PHImageManager.defaultManager()
        let requestOptions = PHImageRequestOptions()
        requestOptions.synchronous = true
        
        let item = photosOfLocalGallery?.objectAtIndex(index) as! PHAsset
        imageManager.requestImageForAsset(item, targetSize: CGSize(width: item.pixelWidth / 2, height: item.pixelHeight / 2), contentMode: .AspectFill, options: requestOptions, resultHandler: { (image: UIImage?, info: [NSObject : AnyObject]?) in
            if image != nil {
                self.imgviewSelectedPhoto.image = image
                self.imgviewBlurBackground.image = image
                self.selectedFile = image
            }
        })

    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.allowsEditing = false
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.imgviewSelectedPhoto.image = image
        self.imgviewBlurBackground.image = image
        self.selectedFile = image
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    // MARK: delegate
    
    // MARK: - Button
    
    
    
    // MARK: - Navigation
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SEGUE.GOTOFinalPostVC {
            let finalPostVC = segue.destinationViewController as! UMOUploadFinalPostVC
            finalPostVC.postImage = self.selectedFile as? UIImage
        }
    }
}
