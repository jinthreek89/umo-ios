//
//  UMOMainTabBarController.swift
//  UMO
//
//  Created by Boris on 7/3/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase

class UMOMainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().tintColor = APPCOLOR.LIGHT_BLUE
        
    }
    
}
