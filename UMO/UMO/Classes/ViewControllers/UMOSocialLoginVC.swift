//
//  UMOSocialLoginVC.swift
//  UMO
//
//  Created by Boris on 6/30/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import InstagramKit
import TwitterKit
import PinterestSDK

class UMOSocialLoginVC: UMOLoginBaseVC, UITableViewDataSource, UITableViewDelegate, UMOSocialAccountCellDelegate {

    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var tblSocialAccounts: UITableView!
    
    
    
    private let ICONKEY  = "icon"
    private let TITLEKEY = "title"
    
    private var _arraySocialAccounts = [Dictionary<String, String>]()
    private var _arrayConnectedSocialAccount = [Bool]()
    
    struct SocialLoginState {
        static let None = 0
        static let Instagram = 1
        static let Twitter = 2
        static let Pinterest = 3
        static let Snapchat = 4
        static let Facebook = 5
    }
    
    private var socialLoginState = SocialLoginState.None
    
    override func viewDidLoad() {
        super.viewDidLoad()

        _arraySocialAccounts = [
            [ICONKEY : "iconInstagram", TITLEKEY : "Instagram"],
            [ICONKEY : "iconTwitter",   TITLEKEY : "Twitter"],
            [ICONKEY : "iconSnapChat",  TITLEKEY : "Snapchat"],
            [ICONKEY : "iconPinterest", TITLEKEY : "Pinterest"],
            [ICONKEY : "iconFacebook",  TITLEKEY : "Facebook"],
        ]
        
        _arrayConnectedSocialAccount = [false, false, false, false, false]
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setupLayout() {
        super.setupLayout()
        makeRoundView(self.btnRegister)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        checkLoginState()
    }

    // MARK: - UITableView datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return _arraySocialAccounts.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UMOSocialAccountCell = tableView.dequeueReusableCellWithIdentifier(UMOSocialAccountCell.identifier) as! UMOSocialAccountCell
        let menuItem = _arraySocialAccounts[indexPath.row]
        cell.imgviewIcon.image = UIImage.init(named: menuItem[ICONKEY]! as String)
        cell.btnSocia.setTitle(menuItem[TITLEKEY]! as String, forState: .Normal)
        cell.imgviewCheckmark.hidden = !_arrayConnectedSocialAccount[indexPath.row]
        cell.delegate = self;
        
        
        
        return cell
    }
    
    // MARK: delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    
    // MARK: UMOSocialAccountCellDelegate
    func UMOSocialAccountCellDidTapButton(cell: UMOSocialAccountCell) {
        print("press account button")
        if let indexPath = self.tblSocialAccounts.indexPathForCell(cell) {
            let menuItem = _arraySocialAccounts[indexPath.row]
            var loginTypeString: String!
            if menuItem[TITLEKEY]?.lowercaseString == "facebook" {
                socialLoginState = SocialLoginState.Facebook
                loginWithFacebook()
                loginTypeString = "Loggin in Facebook..."
            }
            else if menuItem[TITLEKEY]?.lowercaseString == "twitter" {
                socialLoginState = SocialLoginState.Twitter
                loginWithTwitter()
                loginTypeString = "Loggin in Twitter..."
            }
            else if menuItem[TITLEKEY]?.lowercaseString == "instagram" {
                socialLoginState = SocialLoginState.Instagram
                loginWithInstagram()
                loginTypeString = "Loggin in Instagram..."
            }
            else if menuItem[TITLEKEY]?.lowercaseString == "pinterest" {
                socialLoginState = SocialLoginState.Pinterest
                loginWithPinterest()
                loginTypeString = "Loggin in Pinterest..."
            }
            else if menuItem[TITLEKEY]?.lowercaseString == "snapchat" {
                socialLoginState = SocialLoginState.Snapchat
                loginWithSnapchat()
                loginTypeString = "Loggin in Snapchat..."
            }
            
            showLoadingIndicator(loginTypeString)
        }
    }
    
    // MARK: - Social Login
    
    func loginWithFacebook() -> Void {
        _ = FBSDKLoginManager.init().logInWithReadPermissions(["public_profile", "email"], fromViewController: self, handler: { (result: FBSDKLoginManagerLoginResult!, error: NSError!) in
            if error != nil {
                print("Facebook login error : \(error.localizedDescription)")
//                self.checkLoginState()
                self.socialLoginState = SocialLoginState.None
                self.hideLoadingIndicator()

            } else if result.isCancelled {
                print("Facebook login canceled")
//                self.checkLoginState()
                self.socialLoginState = SocialLoginState.None
                self.hideLoadingIndicator()

            } else {
                print("Facebook login succeeded. Now loading user information ... ")
                
                FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields": "id,name,email"]).startWithCompletionHandler({ (connection: FBSDKGraphRequestConnection!, result: AnyObject!, error: NSError!) in
                    if error == nil {
                        let email: String? = result["email"] as? String
                        let fbID: String = result["id"] as! String
                        let name: String = result["name"] as! String
                        
                        print("email : \(email),   fbid: \(fbID),   name: \(name)")
                    } else {
                        print("Facebook user information loading error : \(error.localizedDescription)")
                    }
//                    self.checkLoginState()
                    self.facebookLoginCheck()
                    
                })
                
                
                
            }
        })
    }
    
    func loginWithTwitter() -> Void {
        
        Twitter.sharedInstance().logInWithCompletion { session, error in
            if (session != nil) {
                print("Twitter signed in as \(session!.userName)");
                self.appManager().socialLoginSuccessTwitter = true
                self._arrayConnectedSocialAccount[1] = true
                self.tblSocialAccounts.reloadData()

            } else {
                print("Twitter error: \(error!.localizedDescription)");
            }
            self.hideLoadingIndicator()
        }
    }
    
    func loginWithInstagram() -> Void {
        
        self.performSegueWithIdentifier(SEGUE.GOTOInstagramLoginNC, sender: self)
        
        /*
        let instagramManager = appManager().instagram
        instagramManager.sessionDelegate = self
        if instagramManager.isSessionValid() {
            
        } else {
            instagramManager.authorize(["comments", "likes"])
        }
 */
    }
    
    func loginWithPinterest() -> Void {
        
        PDKClient.sharedInstance().authenticateWithPermissions([PDKClientReadPublicPermissions,
                                                                PDKClientWritePublicPermissions,
                                                                PDKClientReadPrivatePermissions,
                                                                PDKClientWritePrivatePermissions,
                                                                PDKClientReadRelationshipsPermissions,
                                                                PDKClientWriteRelationshipsPermissions],
        withSuccess: { (responseObject: PDKResponseObject!) in
            let user: PDKUser = responseObject.user()
            print("Pinterest login : \(user.firstName)")
            self.appManager().socialLoginSuccessPinterest = true
            self._arrayConnectedSocialAccount[3] = true
            self.tblSocialAccounts.reloadData()

            self.hideLoadingIndicator()
        }) { (error : NSError!) in
            print("Pinterest error : \(error.localizedDescription)")
            self.hideLoadingIndicator()
        }
        
//        performSelector(#selector(hideLoadingIndicator), withObject: nil, afterDelay: 0.5)
    }
    
    func loginWithSnapchat() -> Void {
        performSelector(#selector(hideLoadingIndicator), withObject: nil, afterDelay: 0.5)

    }
    
    func checkLoginState() -> Void {
        switch socialLoginState {
        case SocialLoginState.Facebook:
            
            break
        case SocialLoginState.Twitter:
            
            break
        case SocialLoginState.Instagram:
            instagramLoginCheck()
            
            break
        case SocialLoginState.Pinterest:
            
            break
        case SocialLoginState.Snapchat:
            
            break
        default:
            break
        }
        

    }
    
    // MARK: - Instagram
    func instagramLoginCheck() -> Void {
        if let instagramAccessToken = InstagramEngine.sharedEngine().accessToken {
            print(instagramAccessToken)
            appManager().socialLoginSuccessInstagram = true
            _arrayConnectedSocialAccount[0] = true
            self.tblSocialAccounts.reloadData()
            
        }
        socialLoginState = SocialLoginState.None
        hideLoadingIndicator()

    }
    
    // MARK: - Twitter
    
    // MARK: - Snapchat
    
    // MARK: - Pinterest
    
    // MARK: - Facebook
    func facebookLoginCheck() -> Void {
        appManager().socialLoginSuccessFacebook = true
        _arrayConnectedSocialAccount[4] = true
        self.tblSocialAccounts.reloadData()

        socialLoginState = SocialLoginState.None
        hideLoadingIndicator()

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
