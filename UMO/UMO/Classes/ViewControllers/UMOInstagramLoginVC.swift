//
//  UMOInstagramLoginVC.swift
//  UMO
//
//  Created by Boris on 7/12/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import InstagramKit

class UMOInstagramLoginVC: UMOViewController, UIWebViewDelegate {

    @IBOutlet weak var webviewInstagram: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem?.enabled = false
        let authURL = InstagramEngine.sharedEngine().authorizationURL()
        self.webviewInstagram.loadRequest(NSURLRequest(URL: authURL))
        
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        do {
            try InstagramEngine.sharedEngine().receivedValidAccessTokenFromURL(request.URL!)
            successLogin()
        } catch {
            
        }
        return true
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.navigationItem.rightBarButtonItem?.enabled = true
    }
    
    func successLogin() -> Void {
        print("Success login with Instagram")
        self.navigationController?.dismissViewControllerAnimated(true, completion: {
            
        })
    }
    
    @IBAction func onPressDone(sender: AnyObject) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: { 
            
        })
    }
    
}
