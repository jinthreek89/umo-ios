//
//  UMOPurchaseDetailVC.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import MZFormSheetPresentationController

class UMOPurchaseDetailVC: UMOTableViewController {

    @IBOutlet weak var imgviewThumbnail: UIImageView!
    @IBOutlet weak var imgviewUserAvatar: UIImageView!
    @IBOutlet weak var lblUserFullname: UILabel!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnMoreDescription: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnBuy: UIButton!
    
    private var _feed: UMOPostModel!
    private var _heightOfDescription: CGFloat = 80
    
    override func setupLayout() {
        super.setupLayout()
        
        makeRoundView(self.btnBuy)
        makeRoundView(self.imgviewUserAvatar, radius: self.imgviewUserAvatar.frame.size.width * 0.5)
        
        self.showFeedInformation()
        
        self.calculateDescriptionHeight()
    }
    
    func setFeed(feed: UMOPostModel) -> Void {
        _feed = feed
    }
    
    private func showFeedInformation() -> Void {
        let placeholder = PLACEHOLDER.FEED(_feed.type!)
        if let thumbnailPath = _feed.thumbnail {
            if let thumbnailURL = NSURL(string: thumbnailPath) {
                self.imgviewThumbnail.sd_setImageWithURL(thumbnailURL, placeholderImage: placeholder)
            } else {
                self.imgviewThumbnail.image = placeholder
            }
        } else {
            self.imgviewThumbnail.image = placeholder
        }
        
        let placeholderAvatar = PLACEHOLDER.AVATAR
        if let avatarPath = _feed.userAvatar {
            if let avatarURL = NSURL(string: avatarPath) {
                self.imgviewUserAvatar.sd_setImageWithURL(avatarURL, placeholderImage: placeholderAvatar)
            } else {
                self.imgviewUserAvatar.image = placeholderAvatar
            }
        } else {
            self.imgviewUserAvatar.image = placeholderAvatar
        }
        
        self.lblUserFullname.text = _feed.userFullName
        self.lblCredits.text = String(_feed.credits!) + " Cr"
        self.lblDescription.text = _feed.mediaDescription

    }
    
    private func calculateDescriptionHeight() -> Void {
//        let str = subTitle as NSString
        if _feed.mediaDescription != nil {
            let attr = [NSFontAttributeName: self.lblDescription.font]
            let width = UIScreen.mainScreen().bounds.size.width - 8 * 2 - 20 * 2
            let size = CGSize(width: width, height: CGFloat.max)
            let rect = _feed.mediaDescription!.boundingRectWithSize(size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes:attr, context:nil)
            _heightOfDescription = rect.size.height
            if _heightOfDescription > 80 - 8 - 20 {
                _heightOfDescription += 40
                self.btnMoreDescription.hidden = false
            } else {
                _heightOfDescription = 80
                self.btnMoreDescription.hidden = true
            }
            
        }
    }
    
    // MARK: - UITableView datasourcce
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 2 {
            if self.btnMoreDescription.selected {
                return _heightOfDescription
            } else {
                return 80
            }
        } else {
            return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        }
    }
    
    // MARK: - Button actions
    
    @IBAction func onPressMoreDescription(sender: AnyObject) -> Void {
        self.btnMoreDescription.selected = !self.btnMoreDescription.selected
//        self.tableView.reloadRowsAtIndexPaths([NSIndexPath.init(forRow: 2, inSection: 0)], withRowAnimation: .Bottom)
        self.tableView.reloadData()
    }
    
    @IBAction func onPressBuy(sender: AnyObject) {
        let temp = random() % 2
        if temp == 0 {
            self.performSegueWithIdentifier(SEGUE.GOTOOutOfCreditsVC, sender: self)
        } else {
            self.performSegueWithIdentifier(SEGUE.GOTOConfirmPurchaseVC, sender: self)
        }
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            if identifier == SEGUE.GOTOOutOfCreditsVC {
                let presentationSegue = segue as! MZFormSheetPresentationViewControllerSegue
//                presentationSegue.formSheetPresentationController.presentationController?.shouldApplyBackgroundBlurEffect = true
                presentationSegue.formSheetPresentationController.contentViewControllerTransitionStyle = .Bounce
                presentationSegue.formSheetPresentationController.presentationController?.shouldDismissOnBackgroundViewTap = true
//                let presentedViewController = presentationSegue.formSheetPresentationController.contentViewController as! UMOOutOfCreditsVC
  
            }
        }
    }

    
}
