//
//  UMOMainBaseCollectionViewController.swift
//  UMO
//
//  Created by Boris on 7/4/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOMainBaseCollectionVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayout()
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .Default
    }
    
    override func setupLayout() {
        super.setupLayout()
        
        addCustomNavigationBar()
        
        collectionView?.contentInset = UIEdgeInsets.init(top: 5, left: 5, bottom: 0, right: 5)

    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 3 - 8
        return CGSizeMake(width, width)
    }

}
