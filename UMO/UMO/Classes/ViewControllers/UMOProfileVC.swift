//
//  UMOProfileVC.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase
import KMPlaceholderTextView
import MZFormSheetPresentationController
import SVProgressHUD

class UMOProfileVC: UMOTableViewController, UMOUploadSelectResourceDelegate, UMOThumbnailPickerDelegate {

    @IBOutlet weak var viewGeneralUserInfoBox: UMOShadowView!
    @IBOutlet weak var viewUserInfoBox: UIView!
    @IBOutlet weak var viewFollowingBox: UIView!
    @IBOutlet weak var viewLastestPosts: UIView!
    @IBOutlet weak var btnProfileEdit: UIButton!
    @IBOutlet weak var imgviewUserAvatar: UIImageView!
    @IBOutlet weak var btnChangePhoto: UIButton!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var txtUserFullname: UITextField!
    @IBOutlet weak var txtUserAbout: KMPlaceholderTextView!
    
    @IBOutlet weak var lblUserFollowers: UILabel!
    @IBOutlet weak var lblUserPosts: UILabel!
    @IBOutlet weak var lblUserFavs: UILabel!
    @IBOutlet weak var lblUserFollowings: UILabel!
    @IBOutlet weak var clsviewLastestPosts: UICollectionView!
    
    @IBOutlet weak var switchPublisher: UISwitch!
    @IBOutlet weak var btnPrevYear: UIButton!
    @IBOutlet weak var btnNextYear: UIButton!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var viewChartBox: UIView!
    
    @IBOutlet weak var viewConnectDropbox: UIView!
    @IBOutlet weak var viewConnectBox: UIView!
    
    @IBOutlet weak var viewTermsOfConditions: UIView!
    @IBOutlet weak var viewPrivacyAndPolicy: UIView!
    @IBOutlet weak var viewAbout: UIView!
    
    @IBOutlet weak var viewSetting: UIView!
    @IBOutlet weak var viewChangePassword: UIView!
    @IBOutlet weak var viewDeleteAccount: UIView!
    @IBOutlet weak var viewRowLogout: UIView!
    
    private var _isChangedProfileInfo = false
    
    private var _userInfo: UMOUserModel?
    private var _imgAvatar: UIImage?
    
    override func setupLayout() {
        super.setupLayout()
        
        addCustomNavigationBar()
        
        makeRoundView(self.imgviewUserAvatar, radius: self.imgviewUserAvatar.frame.size.width * 0.5)
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.txtUserFullname.frame.size.height))
        self.txtUserFullname.leftView = paddingView
        self.txtUserFullname.leftViewMode = .Always
        
        self.txtUserFullname.layer.borderWidth = 1
        self.txtUserAbout.layer.borderWidth    = 1
        self.editProfile(false)
        
        let tapLogout = UITapGestureRecognizer(target: self, action: #selector(UMOProfileVC.logout))
        self.viewRowLogout.addGestureRecognizer(tapLogout)
        
        self.btnFollow.layer.borderWidth = 1
        self.btnFollow.layer.borderColor = self.btnFollow.titleLabel?.textColor.CGColor
        
        if self._userInfo == nil {
            self.btnFollow.hidden = true
            self.btnProfileEdit.hidden = false
            showUserInfo(appManager().currentUser!)
        } else {
            self.btnFollow.hidden = false
            self.btnProfileEdit.hidden = true
            showUserInfo(self._userInfo!)
        }
    }
    
    func setUserInfo(user: UMOUserModel?) -> Void {
        _userInfo = user
    }
    
    private func showUserInfo(user: UMOUserModel) -> Void {
        self.txtUserFullname.text = user.fullname
        self.txtUserAbout.text = user.about
        
        let placeholder = PLACEHOLDER.AVATAR
        if let avatarPath = user.avatar {
            if let avatarURL = NSURL(string: avatarPath) {
                self.imgviewUserAvatar.sd_setImageWithURL(avatarURL, placeholderImage: placeholder)
            } else {
                self.imgviewUserAvatar.image = placeholder
            }
        } else {
            self.imgviewUserAvatar.image = placeholder
        }
        
        if user.followers != nil {
            self.lblUserFollowers.text = String(user.followers!.count)
        } else {
            self.lblUserFollowers.text = "0"
        }
        if user.followings != nil {
            self.lblUserFollowings.text = String(user.followings!.count)
        } else {
            self.lblUserFollowings.text = "0"
        }
        if user.posts != nil {
            self.lblUserPosts.text = String(user.posts!.count)
        } else {
            self.lblUserPosts.text = "0"
        }
        if user.favs != nil {
            self.lblUserFavs.text = String(user.favs!.count)
        } else {
            self.lblUserFavs.text = "0"
        }
        

    }
    
    // MARK: - Menu click events
    
    @IBAction func onPressEditProfile(sender: AnyObject) -> Void {
        if self.btnProfileEdit.selected {
            // save profile
            let currentUser = appManager().currentUser
            if currentUser?.fullname != self.txtUserFullname.text {
                _isChangedProfileInfo = true
            }
            if currentUser?.about != self.txtUserAbout.text {
                _isChangedProfileInfo = true
            }
            if _isChangedProfileInfo {
                self.btnProfileEdit.enabled = false
                saveProfile({
                    self.btnProfileEdit.enabled = true
                })
            }
            

        } else {
            // edit 
            
        }
        self.btnProfileEdit.selected = !self.btnProfileEdit.selected
        self.editProfile(self.btnProfileEdit.selected)
    }
    
    func logout(sender: AnyObject) -> Void {
        let alertView = UIAlertController(title: "Log out", message: "Would you log out?", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "Logout", style: .Default) { (action: UIAlertAction) in
            do {
                try FIRAuth.auth()?.signOut()
                self.navigationController?.dismissViewControllerAnimated(true, completion: { 
                    
                })
            } catch {
                
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertView.addAction(okAction)
        alertView.addAction(cancelAction)
        presentViewController(alertView, animated: true, completion: nil)
        
    }
    
    func onPressTermsOfConditions(sender: AnyObject) -> Void {
        
    }
    
    func onPressPrivacyOfPolicy(sender: AnyObject) -> Void {
        
    }
    
    func onPressAboutApp(sender: AnyObject) -> Void {
        
    }
    
    func onPressSetting(sender: AnyObject) -> Void {
        
    }
    
    func onPressChangePassword(sender: AnyObject) -> Void {
        
    }
    
    func onPressDeleteAccount(sender: AnyObject) -> Void {
        
    }
    
    // MARK: - Edit Profile
    
    func editProfile(edit: Bool) -> Void {
        if edit {  // editable
            self.btnChangePhoto.hidden = false
            self.txtUserFullname.enabled = true
            self.txtUserAbout.editable = true
            self.txtUserFullname.layer.borderColor = UIColor.init(white: 0, alpha: 0.05).CGColor
            self.txtUserAbout.layer.borderColor = UIColor.init(white: 0, alpha: 0.05).CGColor
            
            
        } else {  // save
            self.btnChangePhoto.hidden = true
            self.txtUserFullname.enabled = false
            self.txtUserAbout.editable = false
            self.txtUserFullname.layer.borderColor = UIColor.clearColor().CGColor
            self.txtUserAbout.layer.borderColor = UIColor.clearColor().CGColor
        }
    }
    
    @IBAction func onPressChangePhoto(sender: UIButton) -> Void {
        
        
        appManager().thumbnailResource = UPLOADRESOURCE.NONE
        let selectResourceVC: UMOUploadSelectResourceVC = self.storyboard!.instantiateViewControllerWithIdentifier(UMOUploadSelectResourceVC.identifier) as! UMOUploadSelectResourceVC
        //        selectResourceVC.uploadFinalVC = self
        selectResourceVC.typeForThumbnail = true
        selectResourceVC.delegate = self
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: selectResourceVC)
        formSheetController.presentationController?.contentViewSize = CGSizeMake(300, 300)  // or pass in UILayoutFittingCompressedSize to size automatically with auto-layout
        //        formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = true
        //        formSheetController.presentationController?.blurEffectStyle = UIBlurEffectStyle.Dark
        formSheetController.contentViewControllerTransitionStyle = .SlideAndBounceFromBottom
        
        self.presentViewController(formSheetController, animated: true, completion: nil)
        
    }
    
    func UMOUploadSelectResourceView(vc: UMOUploadSelectResourceVC, resourceType: String) {
        performSelector(#selector(UMOProfileVC.presentThumbnailPickerVC), withObject: nil, afterDelay: 0.1)
    }
    
    func presentThumbnailPickerVC() -> Void {
        if appManager().thumbnailResource != UPLOADRESOURCE.NONE {
            let thumbnailPickerVC: UMOThumbnailPickerVC = self.storyboard?.instantiateViewControllerWithIdentifier("UMOThumbnailPickerVC") as! UMOThumbnailPickerVC
            thumbnailPickerVC.delegate = self
            thumbnailPickerVC.resourceType = appManager().thumbnailResource
            self.presentViewController(thumbnailPickerVC, animated: true, completion: {
                
            })
        }
    }
    
    func UMOThumbnailPickerView(vc: UMOThumbnailPickerVC, image: UIImage?) {
        if image != nil {
            self.imgviewUserAvatar.image = image
            _imgAvatar = image
            _isChangedProfileInfo = true
        }
    }

    func saveProfile(complete:(() -> Void)?) -> Void {
        
        let saveProfile = { (fullname: String, about: String?, avatar: String?) in
            let databaseRef = self.databaseRef().child(TABLENAME.USERINFO).child((self.appManager().currentUser?.UID)!)
            var profileDic: [String: String] = [
                FIELDNAME.USER_FULLNAME : fullname
            ]
            if about != nil {
                profileDic[FIELDNAME.USER_ABOUT] = about!
            }
            if avatar != nil {
                profileDic[FIELDNAME.USER_AVATAR] = avatar!
            }
            databaseRef.updateChildValues(profileDic)
            if complete != nil {
                complete!()
            }
        }
        
        if _imgAvatar != nil {

            self.navigationController?.view.userInteractionEnabled = false

            let fileRef = storageRef().child(STORAGEPATH.AVATAR).child((appManager().currentUser?.UID)! + ".png")
            let uploadComplete = { (metaData: FIRStorageMetadata?, error: NSError?) in
                if error == nil {
                    saveProfile(self.txtUserFullname.text!, self.txtUserAbout.text, metaData?.downloadURL()?.absoluteString)
                } else {
                    saveProfile(self.txtUserFullname.text!, self.txtUserAbout.text, nil)
                }
                
                self.navigationController?.view.userInteractionEnabled = true
                SVProgressHUD.dismiss()
            }
            
            let imageData = UIImagePNGRepresentation(_imgAvatar!)
            let metaData = FIRStorageMetadata()
            metaData.contentType = "image/png"
            let uploadTask: FIRStorageUploadTask = fileRef.putData(imageData!, metadata: metaData, completion: uploadComplete)
            
            uploadTask.observeStatus(.Progress) { snapshot in
                // Upload reported progress
                if let progress = snapshot.progress {
                    let percentComplete = Float(progress.completedUnitCount) / Float(progress.totalUnitCount)
                    SVProgressHUD.showProgress(percentComplete)
                    print(percentComplete)
                }
            }

        } else {
            saveProfile(self.txtUserFullname.text!, self.txtUserAbout.text, nil)
        }
        
    }
    
    // MARK: - Following check
    
}
