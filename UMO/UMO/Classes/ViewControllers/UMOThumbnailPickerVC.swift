//
//  UMOThumbnailPickerVC.swift
//  UMO
//
//  Created by Boris on 7/14/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOThumbnailPickerVC: UMOUploadPhotoPickerVC {
    
    var delegate: UMOThumbnailPickerDelegate?
    
    var uploadFinalVC: UMOUploadFinalPostVC!
    
    
    override func onPressNext(sender: AnyObject) {
        
        /*
        if self.selectedFile != nil {
            var thumbnailImage = self.selectedFile as! UIImage
            if thumbnailImage.size.width > 1024 {
                let dt = 1024 / thumbnailImage.size.width
                let height = thumbnailImage.size.height * dt
                thumbnailImage = thumbnailImage.resize(CGSize(width: 1024, height: height), contentMode: UIImageContentMode.ScaleAspectFill)!
            }
            
            uploadFinalVC.setThumbnailImage(thumbnailImage)
//            uploadFinalVC.imgviewThumnail.image = self.selectedFile as? UIImage
        }
        */
        
        delegate?.UMOThumbnailPickerView(self, image: self.selectedFile as? UIImage)
        
        onCancel(sender)
    }
    
}


protocol UMOThumbnailPickerDelegate: NSObjectProtocol {
    func UMOThumbnailPickerView(vc: UMOThumbnailPickerVC, image: UIImage?) -> Void
}