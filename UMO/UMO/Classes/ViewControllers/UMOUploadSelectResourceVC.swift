//
//  UMOUploadSelectResourceVC.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOUploadSelectResourceVC: UMOUploadBaseVC {

    static let identifier = "UMOUploadSelectResourceVC"
    
    @IBOutlet weak var viewGalleryBox: UMOShadowView!
    @IBOutlet weak var viewDropboxBox: UMOShadowView!
    @IBOutlet weak var viewBoxBox: UMOShadowView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    
    var typeForThumbnail = false
    var uploadFinalVC: UMOUploadFinalPostVC?
    var delegate: UMOUploadSelectResourceDelegate?
    
    override func setupLayout() {
        super.setupLayout()
//        makeRoundView(self.viewGalleryBox)
//        makeRoundView(self.viewDropboxBox)
//        makeRoundView(self.viewBoxBox)
        
        let tapGallery = UITapGestureRecognizer(target: self, action: #selector(UMOUploadSelectResourceVC.onPressLocalGallery(_:)))
        self.viewGalleryBox.addGestureRecognizer(tapGallery)
        
        let tapDropbox = UITapGestureRecognizer(target: self, action: #selector(UMOUploadSelectResourceVC.onPressDropbox(_:)))
        self.viewDropboxBox.addGestureRecognizer(tapDropbox)
        
        let tapBox = UITapGestureRecognizer(target: self, action: #selector(UMOUploadSelectResourceVC.onPressBox(_:)))
        self.viewBoxBox.addGestureRecognizer(tapBox)
        
        var title: String!
        if typeForThumbnail == true {
            title = "Upload Thumbnail Image"
            btnBack.setTitle("Cancel", forState: .Normal)
            btnBack.setImage(nil, forState: .Normal)
        } else {
        
            title = "Upload \(appManager().uploadType) file"
            
        }
        
        self.lblTitle.text = title
    }
    
    func onPressLocalGallery(sender: AnyObject) -> Void {
        if typeForThumbnail == true {
            appManager().thumbnailResource = UPLOADRESOURCE.LOCAL
        } else {
            appManager().uploadResource = UPLOADRESOURCE.LOCAL
        }
        self.openUploadPicker(sender)
    }
    
    func onPressDropbox(sender: AnyObject) -> Void {
        if typeForThumbnail == true {
            appManager().thumbnailResource = UPLOADRESOURCE.DROPBOX
        } else {
            appManager().uploadResource = UPLOADRESOURCE.DROPBOX
        }
        self.openUploadPicker(sender)
    }

    func onPressBox(sender: AnyObject) -> Void {
        if typeForThumbnail == true {
            appManager().thumbnailResource = UPLOADRESOURCE.BOX
        } else {
            appManager().uploadResource = UPLOADRESOURCE.BOX
        }
        self.openUploadPicker(sender)
    }

    func onPressCloudDrive(sender: AnyObject) -> Void {
        self.openUploadPicker(sender)
    }
    
    private func openUploadPicker(sender: AnyObject) -> Void {
        if typeForThumbnail == true {
            self.dismissViewControllerAnimated(true, completion: {
                self.delegate?.UMOUploadSelectResourceView(self, resourceType: self.appManager().thumbnailResource)
//                self.uploadFinalVC?.showThumbnailPickerVC()
            })
        } else {
            self.performSegueWithIdentifier(SEGUE.GOTOUploadPickerNC, sender: sender)
        }
    }
    
    override func onBack(sender: AnyObject) {
        if self.navigationController == nil {
            self.dismissViewControllerAnimated(true, completion: { 
                
            })
        } else {
            super.onBack(sender)
        }
    }
    

}

protocol UMOUploadSelectResourceDelegate {
    func UMOUploadSelectResourceView(vc: UMOUploadSelectResourceVC, resourceType: String) -> Void
}