//
//  UMOMainBaseVC.swift
//  UMO
//
//  Created by Boris on 7/3/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOMainBaseVC: UMOViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .Default
    }

    override func setupLayout() {
        super.setupLayout()
        
        addCustomNavigationBar()
    }
    
    
    
}
