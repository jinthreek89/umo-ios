//
//  UMOHomeVC.swift
//  UMO
//
//  Created by Boris on 6/30/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Firebase

class UMOHomeVC: UMOLoginBaseVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
//    @IBOutlet weak var viewDescription: UIView!
//    @IBOutlet weak var lblDescription: UIView!
    @IBOutlet weak var pageIndicator: UIPageControl!
    
    private var player: AVPlayer?
    private var playerLayer: AVPlayerLayer?
    
    private let VIDEOKEY        = "videoURL"
    private let DESCRIPTIONKEY  = "description"
    private var _arraySplashDescriptions = [Dictionary<String, String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        _arraySplashDescriptions = [
            [VIDEOKEY : "video1",   DESCRIPTIONKEY : NSLocalizedString("Bushwick meh Blue Bottle pork belly mustache skateboard.", comment: "")],
            [VIDEOKEY : "video2",   DESCRIPTIONKEY : NSLocalizedString("Bushwick meh Blue Bottle pork belly mustache skateboard2.", comment: "")],
            [VIDEOKEY : "video1",   DESCRIPTIONKEY : NSLocalizedString("Bushwick meh Blue Bottle pork belly mustache skateboard3.", comment: "")],
            [VIDEOKEY : "video2",   DESCRIPTIONKEY : NSLocalizedString("Bushwick meh Blue Bottle pork belly mustache skateboard4.", comment: "")],
            [VIDEOKEY : "video1",   DESCRIPTIONKEY : NSLocalizedString("Bushwick meh Blue Bottle pork belly mustache skateboard5.", comment: "")],
        ]

        let desc1 = _arraySplashDescriptions[0]
        setupVideoBackground(desc1[VIDEOKEY]!)
        
    }
    
    override func setupLayout() {
        super.setupLayout()
//        self.viewDescription.backgroundColor = UIColor.clearColor()
        makeRoundView(self.btnLogin)
        makeRoundView(self.btnRegister)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if FIRAuth.auth()?.currentUser == nil {
            player?.play()
        } else {
            
            let timestamp = FIRServerValue.timestamp()
            print("Server time stamp : \(timestamp)")
            
//            if appManager().fullUserInfo == nil {
            if appManager().currentUser == nil {
                let userUid = (FIRAuth.auth()?.currentUser?.uid)!
//                databaseRef().child(<#T##pathString: String##String#>)
//                var arrayUserInfo: [String : AnyObject] = [FIELDNAME.USER_UID : userUid]
                
                let logoutWithError = { (error: NSError?) in
                    
                    do {
                        try FIRAuth.auth()?.signOut()
                        self.navigationController?.dismissViewControllerAnimated(true, completion: {
                            
                        })
                    } catch {
                        self.navigationController?.dismissViewControllerAnimated(true, completion: {
                            
                        })

                    }
                    
                }
                
                databaseRef().child(TABLENAME.USERINFO).child(userUid).observeEventType(.Value, withBlock: { (snapshot: FIRDataSnapshot) in
                    
                    if snapshot.childrenCount > 0 {
                        self.appManager().currentUser = UMOUserModel(snapshot: snapshot)
                    
                        print(self.appManager().currentUser?.getDictionaryData())
                    } else {
                        logoutWithError(nil)
                    }
                    
                }, withCancelBlock: logoutWithError)
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        player?.pause()
    }
    
    private func setupVideoBackground(videoFileName: String) {
        
        let videoURL: NSURL = NSBundle.mainBundle().URLForResource(videoFileName, withExtension: "mp4")!
        
        player = AVPlayer(URL: videoURL)
        player?.actionAtItemEnd = .None
        player?.muted = true
        
        if playerLayer != nil {
            playerLayer!.removeFromSuperlayer()
            playerLayer = nil
        }
        playerLayer = AVPlayerLayer(player: player)
        playerLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer!.zPosition = -1
        
        playerLayer!.frame = view.frame
        
        view.layer.addSublayer(playerLayer!)
        
        player?.play()
        
        //loop video
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(UMOHomeVC.loopVideo),
                                                         name: AVPlayerItemDidPlayToEndTimeNotification,
                                                         object: nil)

    }

    func loopVideo() {
        player?.seekToTime(kCMTimeZero)
        player?.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UICollectionView datasourcce
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _arraySplashDescriptions.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("descriptionCell", forIndexPath: indexPath)
        let label: UILabel! = cell.contentView.viewWithTag(10) as! UILabel
        let descriptionForCell = _arraySplashDescriptions[indexPath.row]
        label.text = descriptionForCell[DESCRIPTIONKEY]
        
        return cell;
    }
    
    // MARK:  UICollectionView flowlayout delegate
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let cellSize = collectionView.bounds.size
        return cellSize
    }
    
    // MARK: ScrollView delegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageIndex: Int = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        let descriptionForIndex = _arraySplashDescriptions[pageIndex]
        let videoFileName: String = descriptionForIndex[VIDEOKEY]! as String
        
        setupVideoBackground(videoFileName)
        self.pageIndicator.currentPage = pageIndex
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
