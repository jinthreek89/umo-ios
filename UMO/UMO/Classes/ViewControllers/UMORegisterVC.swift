//
//  UMORegisterVC.swift
//  UMO
//
//  Created by Boris on 6/30/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase


class UMORegisterVC: UMOLoginBaseVC, UITextFieldDelegate {

    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var constraintBottomSpaceOfTextFields : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setupLayout() {
        super.setupLayout()
        makeRoundView(self.btnRegister)
        makeRoundView(self.txtPassword)
        makeRoundView(self.txtUsername)
        makeRoundView(self.txtEmail)
        makeRoundView(self.txtFullName)
        
        addLeftPadding(10, textField: self.txtUsername)
        addLeftPadding(10, textField: self.txtPassword)
        addLeftPadding(10, textField: self.txtEmail)
        addLeftPadding(10, textField: self.txtFullName)

    }
    
    override func keyboardWillShowRect(keyboardSize: CGSize) {
        if CGRectGetMaxY(self.txtPassword.frame) > (self.view.frame.size.height - keyboardSize.height) {
            self.constraintBottomSpaceOfTextFields.constant = keyboardSize.height + 10
            updateConstraintWithAnimate(true)
        }
    }
    
    override func keyboardWillHideRect() {
        if self.constraintBottomSpaceOfTextFields.constant != 180 {
            self.constraintBottomSpaceOfTextFields.constant = 180
            updateConstraintWithAnimate(true)
        }
    }

    @IBAction func onRegister(sender: AnyObject) {
        
        
        
        var missingFields: [String!] = []
        if self.txtFullName.text?.length == 0 {
            missingFields.append("Full Name")
        }
        if self.txtUsername.text?.length == 0 {
            missingFields.append("Unique Username")
        }
        if self.txtEmail.text?.length == 0 {
            missingFields.append("Email")
        }
        if self.txtPassword.text?.length == 0 {
            missingFields.append("Password")
        }
        if missingFields.count > 0 {
            let alertTitle = NSLocalizedString("Missing fields", comment: "")
            var alertMessage: String = ""
            for missingField in missingFields {
                alertMessage += missingField + ", "
            }
            alertMessage = alertMessage.substringToIndex(alertMessage.endIndex.advancedBy(-2))
            
            showAlert(alertTitle, Message: alertMessage, CloseButton: NSLocalizedString("Close", comment: ""), Completion: nil)
            
            return
        }
        
        registerUserName(self.txtUsername.text!, FullName: self.txtFullName.text!, Email: self.txtEmail.text!, Password: self.txtPassword.text!)
        
 
//        self.performSegueWithIdentifier(SEGUE.GOTOSocialLoginVC, sender: self)
    }
    
    // MARK: - UITextifleld delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.txtFullName {
            self.txtUsername.becomeFirstResponder()
        } else if textField == self.txtUsername {
            self.txtEmail.becomeFirstResponder()
        } else if textField == self.txtEmail {
            self.txtPassword.becomeFirstResponder()
        } else if textField == self.txtPassword {
            self.txtPassword.resignFirstResponder()
            self.onRegister(self.btnRegister)
        }
        return true
    }
    
    // MARK: - Register User
    
    func registerUserName(username: String!, FullName fullname: String!, Email email: String!, Password password: String!) -> Void {
        
        showLoadingIndicator(NSLocalizedString("Registering...", comment: ""))
        
        FIRAuth.auth()?.createUserWithEmail(email, password: password, completion: { (user: FIRUser?, error: NSError?) in
            if user != nil {
                let changeRequest = user?.profileChangeRequest()
                changeRequest?.displayName = username
                changeRequest?.commitChangesWithCompletion({ (error: NSError?) in
                    self.stopActivityAnimating()
                    if error == nil {
                        self.performSegueWithIdentifier(SEGUE.GOTOSocialLoginVC, sender: self)
                    }
                })
                
                self.createUserInformation(user!)
                
            } else {
                let errorDescription = error!.localizedDescription
                self.hideLoadingIndicatorWithSimpleAlert(NSLocalizedString("Error", comment: ""), Message: errorDescription, CloseButton: NSLocalizedString("OK", comment: ""), Completion: {
                    
                })
            }
        })
    }
    
    func createUserInformation(user: FIRUser) -> Void {
        
        let userInfo = UMOUserModel(
            email: user.email,
            username: self.txtUsername.text,
            fullname: self.txtFullName.text,
            credits: 10
        )
        userInfo.about = ""
        userInfo.avatar = ""
        userInfo.posts = []
        userInfo.favs  = []
        userInfo.facebook = ""
        userInfo.twitter  = ""
        userInfo.pinterest = ""
        userInfo.instagram = ""
        userInfo.followings = []
        userInfo.followers  = []
        userInfo.libraries  = []
        userInfo.dropbox = ""
        userInfo.box = ""
        
        
//        let userInfo: [String: String] = [FIELDNAME.USER_EMAIL: user.email!,
//                                          FIELDNAME.USER_FULLNAME: self.txtFullName.text!,
//                                          FIELDNAME.USER_USERNAME: self.txtUsername.text!]
        databaseRef().child(TABLENAME.USERINFO).child(user.uid).setValue(userInfo.getDictionaryData());
        userInfo.UID = user.uid
        appManager().currentUser = userInfo
//        appManager().fullUserInfo = userInfo
//        appManager().fullUserInfo![FIELDNAME.USER_UID] = user.uid
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
