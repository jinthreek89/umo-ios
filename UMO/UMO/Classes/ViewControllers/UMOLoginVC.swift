//
//  UMOLoginVC.swift
//  UMO
//
//  Created by Boris on 6/30/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase

class UMOLoginVC: UMOLoginBaseVC, UITextFieldDelegate {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func setupLayout() {
        super.setupLayout()
        makeRoundView(self.btnLogin)
        makeRoundView(self.txtPassword)
        makeRoundView(self.txtUsername)
        
        addLeftPadding(10, textField: self.txtUsername)
        addLeftPadding(10, textField: self.txtPassword)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onLogin(sender: AnyObject) -> Void {
        
//        navigationController?.performSegueWithIdentifier(SEGUE.GOTOMainTabVC, sender: self)

        
        
        var missingFields: [String!] = []
        if self.txtUsername.text?.length == 0 {
            missingFields.append("Email")
        }
        if self.txtPassword.text?.length == 0 {
            missingFields.append("Password")
        }
        if missingFields.count > 0 {
            let alertTitle = NSLocalizedString("Missing fields", comment: "")
            var alertMessage: String = ""
            for missingField in missingFields {
                alertMessage += missingField + ", "
            }
            alertMessage = alertMessage.substringToIndex(alertMessage.endIndex.advancedBy(-2))
            
            showAlert(alertTitle, Message: alertMessage, CloseButton: NSLocalizedString("Close", comment: ""), Completion: nil)
            
            return
        }
        
        loginWithEmail(self.txtUsername.text!, Password: self.txtPassword.text!)

        
        
    }
    
    // MARK: - UITextField delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.txtUsername {
            self.txtPassword.becomeFirstResponder()
        } else if textField == self.txtPassword {
            textField.resignFirstResponder()
            onLogin(self.btnLogin)
        }
        return true
    }
    
    // MARK: - Login
    func loginWithEmail(email: String!, Password password: String! ) -> Void {
        
        showLoadingIndicator(NSLocalizedString("Logging in...", comment: ""))
        FIRAuth.auth()?.signInWithEmail(email, password: password, completion: { (user: FIRUser?, error: NSError?) in
            if user != nil {
                
                self.navigationController?.performSegueWithIdentifier(SEGUE.GOTOMainTabVC, sender: self)

                self.hideLoadingIndicator()
            } else {
                let errorDescription = error!.localizedDescription
                self.hideLoadingIndicatorWithSimpleAlert(NSLocalizedString("Error", comment: ""), Message: errorDescription, CloseButton: NSLocalizedString("OK", comment: ""), Completion: {
                    
                })
                self.hideLoadingIndicator()
            }
        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
