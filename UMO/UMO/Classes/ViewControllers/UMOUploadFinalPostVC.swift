//
//  UMOFinalPostVC.swift
//  UMO
//
//  Created by Boris on 7/8/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase
import KMPlaceholderTextView
import MZFormSheetPresentationController
import BRYXBanner
import SVProgressHUD


class UMOUploadFinalPostVC: UMOTableViewController, UITextViewDelegate, UMOUploadSelectResourceDelegate, UMOThumbnailPickerDelegate {

    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var imgviewPostMedia: UIImageView!
    @IBOutlet weak var txtDescription: KMPlaceholderTextView!
    @IBOutlet weak var viewThumbnailBox: UMOShadowView!
    @IBOutlet weak var imgviewThumnail: UIImageView!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var viewPricingBox: UMOShadowView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnCreditIncrease: UIButton!
    @IBOutlet weak var btnCreditDecrease: UIButton!
    
    @IBOutlet weak var switchInstagram: UISwitch!
    @IBOutlet weak var switchTwitter: UISwitch!
    @IBOutlet weak var switchSnapchat: UISwitch!
    @IBOutlet weak var switchPinterest: UISwitch!
    @IBOutlet weak var switchFacebook: UISwitch!
    
    @IBOutlet var collectionSwitch: [UISwitch]!
    
    var postImage: UIImage?
    var postVideo: NSURL?
    var postAudio: NSURL?
    var postPDF: NSURL?
    
    private var _creditForPost: Int = 2
    
    var uploadProgress: Float = 0.0
    
    override func setupLayout() {
        super.setupLayout()
        
        makeRoundView(self.btnPost)
        makeRoundView(self.btnUpload)
        self.btnUpload.layer.borderColor = self.btnUpload.titleLabel?.textColor.CGColor
        self.btnUpload.layer.borderWidth = 1
        
        self.imgviewThumnail.layer.borderWidth = 1
        self.imgviewThumnail.layer.borderColor = APPCOLOR.GRAY_BLUE.CGColor
        
        if (appManager().uploadType == UPLOADTYPE.IMAGE) && (postImage != nil) {
            self.imgviewPostMedia.image = postImage
        }
        
        if appManager().uploadType == UPLOADTYPE.IMAGE {
            addGestureForFullScreenImage(self.imgviewPostMedia)
        }
        addGestureForFullScreenImage(self.imgviewThumnail)
        
        self.lblPrice.text = String(_creditForPost)
        
        thumbnailViewEnable(false)
        creditPriceViewEnable(false)
        postButtonEnable(false)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // MARK: Enable/Disable other info
    
    private func thumbnailViewEnable(enableView: Bool) -> Void {
        self.viewThumbnailBox.userInteractionEnabled = enableView
        if enableView == true {
            self.viewThumbnailBox.alpha = 1
        } else {
            self.viewThumbnailBox.alpha = 0.3
        }
    }
    
    private func creditPriceViewEnable(enableView: Bool) -> Void {
        self.viewPricingBox.userInteractionEnabled = enableView
        if enableView == true {
            self.viewPricingBox.alpha = 1
        } else {
            self.viewPricingBox.alpha = 0.3
        }
    }
    
    private func postButtonEnable(enableButton: Bool) -> Void {
        self.btnPost.enabled = enableButton
        if enableButton == true {
            self.btnPost.alpha = 1
        } else {
            self.btnPost.alpha = 0.3
        }
    }
    
    // MARK: - UITextView delegate
    
    func textViewDidChange(textView: UITextView) {
        if textView.text.length > 0 {
            thumbnailViewEnable(true)
        } else {
            thumbnailViewEnable(false)
        }
    }
    
    // MARK: - Thumbnail
    
    @IBAction func onPressThumbnailUpload(sender: UIButton) -> Void {
        
        
        appManager().thumbnailResource = UPLOADRESOURCE.NONE
        let selectResourceVC: UMOUploadSelectResourceVC = self.storyboard!.instantiateViewControllerWithIdentifier(UMOUploadSelectResourceVC.identifier) as! UMOUploadSelectResourceVC
//        selectResourceVC.uploadFinalVC = self
        selectResourceVC.typeForThumbnail = true
        selectResourceVC.delegate = self
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: selectResourceVC)
        formSheetController.presentationController?.contentViewSize = CGSizeMake(300, 300)  // or pass in UILayoutFittingCompressedSize to size automatically with auto-layout
//        formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = true
//        formSheetController.presentationController?.blurEffectStyle = UIBlurEffectStyle.Dark
        formSheetController.contentViewControllerTransitionStyle = .SlideAndBounceFromBottom
        
        self.presentViewController(formSheetController, animated: true, completion: nil)

    }
    
    func UMOUploadSelectResourceView(vc: UMOUploadSelectResourceVC, resourceType: String) {
        performSelector(#selector(UMOUploadFinalPostVC.presentThumbnailPickerVC), withObject: nil, afterDelay: 0.1)
    }
    
    func showThumbnailPickerVC() -> Void {
//        performSelector(#selector(UMOUploadFinalPostVC.presentThumbnailPickerVC), withObject: nil, afterDelay: 0.1)
    }
    
    func presentThumbnailPickerVC() -> Void {
        if appManager().thumbnailResource != UPLOADRESOURCE.NONE {
            let thumbnailPickerVC: UMOThumbnailPickerVC = self.storyboard?.instantiateViewControllerWithIdentifier("UMOThumbnailPickerVC") as! UMOThumbnailPickerVC
            thumbnailPickerVC.delegate = self
            thumbnailPickerVC.resourceType = appManager().thumbnailResource
            self.presentViewController(thumbnailPickerVC, animated: true, completion: {
                
            })
        }
    }
    
    func UMOThumbnailPickerView(vc: UMOThumbnailPickerVC, image: UIImage?) {
        if image != nil {
            var thumbnailImage: UIImage!
            if image!.size.width > 1024 {
                let dt = 1024 / image!.size.width
                let height = image!.size.height * dt
                thumbnailImage = image!.resize(CGSize(width: 1024, height: height), contentMode: UIImageContentMode.ScaleAspectFill)!
            } else {
                thumbnailImage = image!
            }

            self.imgviewThumnail.image = thumbnailImage
            creditPriceViewEnable(true)
            postButtonEnable(true)
        }
    }
    
    func setThumbnailImage(image: UIImage!) -> Void {
        
    }
    
    // MARK: - Credit Price
    
    @IBAction func onPressIncreaseCredit(sender: AnyObject) -> Void {
        _creditForPost += 1
        self.lblPrice.text = String(_creditForPost)
    }
    
    @IBAction func onPressDecreaseCredit(sender: AnyObject) -> Void {
        _creditForPost -= 1
        if _creditForPost < 1 {
            _creditForPost = 1
        }
        self.lblPrice.text = String(_creditForPost)
    }
    
    // MARK: - Post
    
    @IBAction func onPressPost(sender: AnyObject) -> Void {
        if self.imgviewThumnail.image == nil {
            self.showNotificationAlert(title: NSLocalizedString("No thumbnail", comment: ""), message: NSLocalizedString("Please select a thumbnail image file.", comment: ""))
            return
        }
        if self.txtDescription.text.length == 0 {
            let alertView = UIAlertController(title: NSLocalizedString("No Description", comment: ""), message: NSLocalizedString("Would you post media without description?", comment: ""), preferredStyle: .Alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .Default, handler: { (alertAction: UIAlertAction) in
                self.uploadMedia()
            })
            let noAction  = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .Cancel, handler: nil)
            alertView.addAction(yesAction)
            alertView.addAction(noAction)
            presentViewController(alertView, animated: true, completion: nil)
            return
        }
        
        self.uploadMedia()
        
    }
    
    func uploadMedia() -> Void {
        
        if appManager().currentUser != nil {
            touchEnable(false)
            
            /*
            var mediaData: [String: AnyObject] = [
    //                FIELDNAME.MEDIA_UID  : databaseRef().child(TABLENAME.MEDIA).childByAutoId().key,
                FIELDNAME.MEDIA_TYPE : appManager().uploadType,
//                    FIELDNAME.MEDIA_USER : appManager().currentUser!.getDictionaryData(),
                FIELDNAME.MEDIA_DESCRIPTION : (self.txtDescription.text.length > 0 ? self.txtDescription.text : ""),
                FIELDNAME.MEDIA_CREDIT : 1,
                FIELDNAME.MEDIA_SOURCE : appManager().uploadResource,
                FIELDNAME.USER_UID     : appManager().currentUser!.UID!,
                FIELDNAME.USER_FULLNAME : appManager().currentUser!.fullname
            ] */
            
            
            /// Create Media File
            var mediaFile: AnyObject
            var mediaFileName: String
            let mediaFileMetaData = FIRStorageMetadata()
            
            if appManager().uploadType == UPLOADTYPE.IMAGE {
                mediaFile = UIImageJPEGRepresentation(self.postImage!, 0.8)!
                mediaFileName  = "\(STORAGEPATH.MEDIA)/\(appManager().uploadType)/\(NSDate().timeIntervalSince1970).jpg"
                mediaFileMetaData.contentType = "image/jpeg"
            } else {
                // TODO: change real path
                mediaFile = UIImageJPEGRepresentation(self.postImage!, 0.8)!
                mediaFileName  = "\(STORAGEPATH.MEDIA)/\(appManager().uploadType)/\(NSDate().timeIntervalSince1970).jpg"
            }
            
            var thumbnailImage = self.imgviewThumnail.image!
            if thumbnailImage.size.width > 640 || thumbnailImage.size.height > 640 {
                thumbnailImage = thumbnailImage.resize(CGSizeMake(640, 640), contentMode: .ScaleAspectFit)!
            }
            let thumbnailFile = UIImageJPEGRepresentation(self.imgviewThumnail.image!, 0.2)!
            let thumbnailFileName = "\(STORAGEPATH.THUMBNAIL)/\(appManager().uploadType)/\(NSDate().timeIntervalSince1970).jpg"
            let thumbnailFileMetaData = FIRStorageMetadata()
            thumbnailFileMetaData.contentType = "image/jpeg"
            
            
    //        mediaData[FIELDNAME.MEDIA_FILE] =
            uploadFile(mediaFile, filePath: mediaFileName, metaData: mediaFileMetaData ) { (downloadFilePathForMedia: String?) in
                if downloadFilePathForMedia != nil {
//                    mediaData[FIELDNAME.MEDIA_FILE] = downloadFilePathForMedia!
                    self.uploadProgress = 0.5
    //                mediaData[FIELDNAME.MEDIA_THUMBNAIL] =
                    self.uploadFile(thumbnailFile, filePath: thumbnailFileName, metaData: thumbnailFileMetaData, complete: { (downloadFilePathForThumbnail: String?) in
                        if downloadFilePathForThumbnail != nil {
//                            mediaData[FIELDNAME.MEDIA_THUMBNAIL] = downloadFilePathForThumbnail
                            let mediaInfo = UMOPostModel(
                                type: self.appManager().uploadType,
                                source: self.appManager().uploadResource,
                                file: downloadFilePathForMedia!,
                                thumbnail: downloadFilePathForThumbnail!,
                                credits: self._creditForPost,
                                userFullName: self.appManager().currentUser!.fullname,
                                userUID: self.appManager().currentUser!.UID!,
                                thumbnailDimention: self.postImage!.size,
                                createDate: NSDate()
                            )
                            mediaInfo.mediaDescription = self.txtDescription.text
                            var mediaData = mediaInfo.getDictionaryData()
                            mediaData[FIELDNAME.MEDIA_CREATEDATE] = FIRServerValue.timestamp()
                            
                            self.databaseRef().child(TABLENAME.MEDIA).childByAutoId().setValue(mediaData, withCompletionBlock: { (error: NSError?, ref: FIRDatabaseReference) in
                                self.appManager().currentUser?.addPost(ref.key)
                            })
    //                        self.databaseRef().childByAppendingPath(TABLENAME.MEDIA).setValue(mediaData)
    //                        NSNotificationCenter.defaultCenter().postNotificationName(APPNOTIFICATION.POSTEDMEDIA, object: nil)
                            
                            SVProgressHUD.showInfoWithStatus("Successfully Posted")
                            
                            self.navigationController?.dismissViewControllerAnimated(true, completion: { 
                                
                            })
                            
                        } else {
                            self.touchEnable(true)
                        }
                        self.uploadProgress = 0
                        
                    })
                } else {
                    self.uploadProgress = 0
                    self.touchEnable(true)
                }
            }
        }
        /// Create Media info
        
    }
    
    private func uploadFile(fileData: AnyObject, filePath: String, metaData: FIRStorageMetadata?, complete: ((String?) -> Void)) -> Void {
        let fileRef = storageRef().child(filePath)
        let uploadComplete = { (metaData: FIRStorageMetadata?, error: NSError?) in
            if error == nil {
                complete(metaData!.downloadURL()?.absoluteString)
            } else {
                complete(nil)
            }
        }
        var uploadTask: FIRStorageUploadTask
        if fileData.isKindOfClass(NSURL) {
            uploadTask = fileRef.putFile(fileData as! NSURL, metadata: metaData, completion: uploadComplete)
        } else {
            uploadTask = fileRef.putData(fileData as! NSData, metadata: metaData, completion: uploadComplete)
        }
        
        uploadTask.observeStatus(.Progress) { snapshot in
            // Upload reported progress
            if let progress = snapshot.progress {
                let percentComplete = Float(progress.completedUnitCount) / Float(progress.totalUnitCount)
                SVProgressHUD.showProgress(percentComplete / 2 + self.uploadProgress)
                print(percentComplete)
            }
        }
//        return fileRef.fullPath
    }
    
    private func touchEnable(enable: Bool) -> Void {
        self.navigationController?.view.userInteractionEnabled = enable
    }
    
    // MARK: - ScrollView delegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.txtDescription.isFirstResponder() == true {
            self.txtDescription.resignFirstResponder()
        }
    }
}
