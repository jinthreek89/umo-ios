//
//  UMOUploadSelectTypeVC.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOUploadSelectTypeVC: UMOUploadBaseVC {

    @IBOutlet weak var viewImageBox: UMOShadowView!
    @IBOutlet weak var viewAudioBox: UMOShadowView!
    @IBOutlet weak var viewVideoBox: UMOShadowView!
    @IBOutlet weak var viewPDFBox: UMOShadowView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UIViewController.notificationPostedMedia(_:)), name: APPNOTIFICATION.POSTEDMEDIA, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: APPNOTIFICATION.POSTEDMEDIA, object: nil)
    }
    
    override func setupLayout() {
        super.setupLayout()
//        makeRoundView(self.viewImageBox)
//        makeRoundView(self.viewAudioBox)
//        makeRoundView(self.viewVideoBox)
//        makeRoundView(self.viewPDFBox)
        
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(UMOUploadSelectTypeVC.onPressImage(_:)))
        self.viewImageBox.addGestureRecognizer(tapImage)
        
        let tapAudio = UITapGestureRecognizer(target: self, action: #selector(UMOUploadSelectTypeVC.onPressAudio(_:)))
        self.viewAudioBox.addGestureRecognizer(tapAudio)
        
        let tapVideo = UITapGestureRecognizer(target: self, action: #selector(UMOUploadSelectTypeVC.onPressVideo(_:)))
        self.viewVideoBox.addGestureRecognizer(tapVideo)

        let tapPDF = UITapGestureRecognizer(target: self, action: #selector(UMOUploadSelectTypeVC.onPressPDF(_:)))
        self.viewPDFBox.addGestureRecognizer(tapPDF)

    }
        
    @IBAction func onPressCancel(sender: AnyObject) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func onPressImage(sender: AnyObject) -> Void {
        appManager().uploadType = UPLOADTYPE.IMAGE
        self.performSegueWithIdentifier(SEGUE.GOTOUploadSelectResourceVC, sender: sender)
    }
    
    func onPressVideo(sender: AnyObject) -> Void {
        appManager().uploadType = UPLOADTYPE.VIDEO
        self.performSegueWithIdentifier(SEGUE.GOTOUploadSelectResourceVC, sender: sender)
    }
    
    func onPressAudio(sender: AnyObject) -> Void {
        appManager().uploadType = UPLOADTYPE.AUDIO
        self.performSegueWithIdentifier(SEGUE.GOTOUploadSelectResourceVC, sender: sender)
    }

    func onPressPDF(sender: AnyObject) -> Void {
        appManager().uploadType = UPLOADTYPE.PDF
        self.performSegueWithIdentifier(SEGUE.GOTOUploadSelectResourceVC, sender: sender)
    }

    override func notificationPostedMedia(sender: AnyObject) {
        performSelector(#selector(UMOUploadSelectTypeVC.dismissSelf), withObject: nil, afterDelay: 0.5)
    }
    
    func dismissSelf() -> Void {
        self.navigationController?.dismissViewControllerAnimated(true, completion: {
            
        })
    }
}
