//
//  UMOFeedMainCell.swift
//  UMO
//
//  Created by Boris on 7/3/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import SDWebImage

class UMOFeedMainCell: UMOCollectionViewCell {

    static let identifier = "UMOFeedMainCell"
    
    @IBOutlet weak var imgviewFeed: UIImageView!
    @IBOutlet weak var viewUserinfo: UIView!
    @IBOutlet weak var viewAvatarBox: UIView!
    
    @IBOutlet weak var imgviewAvatar: UIImageView!
    @IBOutlet weak var lblUsernameShort: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    
    var delegate: UMOFeedMainCellDelegate?
    
    private var _feed: UMOPostModel!
    
    func setFeed(feed: UMOPostModel!) -> Void {
        _feed = feed
        
        let placeholder = PLACEHOLDER.FEED(feed.type!)
        if let thumbnailPath = feed.thumbnail {
            if let thumbnailURL = NSURL(string: thumbnailPath) {
                self.imgviewFeed.sd_setImageWithURL(thumbnailURL, placeholderImage: placeholder)
            } else {
                self.imgviewFeed.image = placeholder
            }
        } else {
            self.imgviewFeed.image = placeholder
        }
        self.lblCartCount.text = String(feed.credits!) + " Cr"

        let placeholderAvatar = PLACEHOLDER.AVATAR
        self.imgviewAvatar.image = placeholderAvatar
        self.lblUsernameShort.text = ""
        
        UMOCache.sharedManager.loadUserInfo(feed.userUID!) { (snapshot) in
            if snapshot != nil {
                self._feed.userInfo = UMOUserModel(snapshot: snapshot!)
                
                if let avatarPath = self._feed.userInfo?.avatar {
                    if let avatarURL = NSURL(string: avatarPath) {
                        self.imgviewAvatar.sd_setImageWithURL(avatarURL, placeholderImage: placeholderAvatar)
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(),{
                    self.lblUsernameShort.text = self._feed.userInfo?.fullname?.shortUserName
                })
                
            }
            print("\(self._feed.UID!) : \(self.lblUsernameShort.text)")
        }
        
    }
    
    func getFeed() -> UMOPostModel {
        return _feed
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewAvatarBox.layer.cornerRadius = self.viewAvatarBox.frame.size.width * 0.5
        self.btnFavorite.selected = false
        
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(UMOFeedMainCell.onPressPhoto(_:)))
        self.imgviewFeed.addGestureRecognizer(tapImage)
        self.imgviewFeed.userInteractionEnabled = true
    }
    
    
    @IBAction func onPressFavorite(sender: AnyObject) {
        self.btnFavorite.selected = !self.btnFavorite.selected
        delegate?.UMOFeedMainCellFavorite(self)
    }
    
    @IBAction func onPressPurchase(sender: AnyObject) {
        delegate?.UMOFeedMainCellPurchase(self)
    }
    
    func onPressPhoto(sender: AnyObject) -> Void {
        delegate?.UMOFeedMainCellTapImage(self)
    }
    
}

protocol UMOFeedMainCellDelegate: NSObjectProtocol {
    func UMOFeedMainCellTapImage(cell: UMOFeedMainCell) -> Void
    func UMOFeedMainCellPurchase(cell: UMOFeedMainCell) -> Void
    func UMOFeedMainCellFavorite(cell: UMOFeedMainCell) -> Void
}