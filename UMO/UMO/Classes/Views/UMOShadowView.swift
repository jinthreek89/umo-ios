//
//  UMOShadowView.swift
//  UMO
//
//  Created by Boris on 7/7/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOShadowView: UMOView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSizeMake(0, 0)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.2
        
    }
}
