//
//  UMOFileCell.swift
//  UMO
//
//  Created by Boris on 7/14/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOFileCell: UMOCollectionViewCell {

    static let identifier = "FileCell"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewCheckMark: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.layer.shadowColor = UIColor.blackColor().CGColor
        self.contentView.layer.shadowOffset = CGSizeMake(0, 0)
        self.contentView.layer.shadowRadius = 1
        self.contentView.layer.shadowOpacity = 0.2

    }
    
}
