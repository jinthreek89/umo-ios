//
//  UMOMainFollowerCell.swift
//  UMO
//
//  Created by Boris on 7/4/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOMainFollowerCell: UMOTableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
    static let identifier = "UMOMainFollowerCell"
    
    @IBOutlet weak var viewMainContainer: UIView!
    @IBOutlet weak var viewProfileBox: UIView!
    @IBOutlet weak var viewFollowerBox: UIView!
    @IBOutlet weak var clsviewPosts: UICollectionView!
    
    @IBOutlet weak var imgviewProfileAvatar: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblUserbio: UILabel!
    @IBOutlet weak var viewProfileStatus: UIView!
    
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblPosts: UILabel!
    @IBOutlet weak var lblFaved: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewMainContainer.layer.shadowOffset    = CGSizeMake(1, 1)
        self.viewMainContainer.layer.shadowRadius    = 1
        self.viewMainContainer.layer.shadowColor     = UIColor.blackColor().CGColor
        self.viewMainContainer.layer.shadowOpacity   = 0.5
        self.imgviewProfileAvatar.layer.cornerRadius = self.imgviewProfileAvatar.frame.size.width * 0.5
        self.viewProfileStatus.layer.cornerRadius    = self.viewProfileStatus.frame.size.width * 0.5
        self.viewProfileStatus.clipsToBounds         = true
        
    }
    
    // MARK: - UICollectionView datasource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: UMOMainFollowerPhotoCell = collectionView.dequeueReusableCellWithReuseIdentifier(UMOMainFollowerPhotoCell.identifier, forIndexPath: indexPath) as! UMOMainFollowerPhotoCell
        
        return cell
    }
    
    // MARK: delegate
    
    // MARK: -
    
    
}

class UMOMainFollowerPhotoCell: UMOCollectionViewCell {
    
    static let identifier = "UMOMainFollowerPhotoCell"
    
    @IBOutlet weak var viewMainContainer: UIView!
    @IBOutlet weak var imgviewFeed: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewMainContainer.layer.shadowColor = UIColor.blueColor().CGColor
        self.viewMainContainer.layer.shadowOffset = CGSizeMake(1, 1)
        self.viewMainContainer.layer.shadowRadius = 2
        self.viewMainContainer.layer.shadowOpacity = 0.7
        
    }
}