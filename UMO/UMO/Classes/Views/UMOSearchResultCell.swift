//
//  UMOSearchResultCell.swift
//  UMO
//
//  Created by Boris on 7/4/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

class UMOSearchResultCell: UMOTableViewCell {
    
    static let Identifier = "UMOSearchResultCell"
    
    @IBOutlet weak var imgvieweAvartar: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgvieweAvartar.layer.cornerRadius = self.imgvieweAvartar.frame.size.width * 0.5
        
    }
    
    
    
    
}
