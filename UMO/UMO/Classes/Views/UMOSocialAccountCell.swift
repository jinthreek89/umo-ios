//
//  UMOSocialAccountCell.swift
//  UMO
//
//  Created by Boris on 6/30/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit

protocol UMOSocialAccountCellDelegate: class {
    
    func UMOSocialAccountCellDidTapButton(cell: UMOSocialAccountCell) -> Void
    
}

class UMOSocialAccountCell: UITableViewCell {

    static let identifier: String = "UMOSocialAccountCell"
    
    @IBOutlet weak var viewMainContainer: UIView!
    @IBOutlet weak var imgviewIcon: UIImageView!
    @IBOutlet weak var btnSocia: UIButton!
    @IBOutlet weak var imgviewCheckmark: UIImageView!
    
    var delegate: UMOSocialAccountCellDelegate?;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewMainContainer.layer.shadowColor = UIColor.blackColor().CGColor
        self.viewMainContainer.layer.shadowRadius = 2
        self.viewMainContainer.layer.shadowOpacity = 0.35
        self.viewMainContainer.layer.shadowOffset = CGSize.init(width: 0, height: 0)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onAccountButton(sender: AnyObject) {
        delegate?.UMOSocialAccountCellDidTapButton(self)
    }
    

}
