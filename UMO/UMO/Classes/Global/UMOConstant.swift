//
//  UMOConstant.swift
//  UMO
//
//  Created by Boris on 7/2/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import Foundation
import UIKit

struct APPKEY {
    
    static let INSTAGRAM_APPID          = "5c63e6031bc3472e80df8523a8ec1118"
    static let INSTAGRAM_SECRETKEY      = "411d0dce7d244714a7f0c6bac4970fe2"
    
    static let FACEBOOK_APPID           = "690884281049658"
    static let FACEBOOK_CLIENTTOKEN     = "ef57e789f499581b5c26ca641a8e2457"
    
    static let TWITTER_CONSUMERKEY      = "tiodbHsr1mCNl9oA7M5cJZfRj"
    static let TWITTER_CONSUMERSECRET   = "vxD98zhTo3mbDC9o3fVGRwFo3DDQawjLNdfHlfUAup33UBoZYI"
    
    static let PINTEREST_APPID          = "4840310445730443060"
    static let PINTEREST_SECRETKEY      = "0cafef96298fa9732e66deea8b45fb5d758a977444548ab4fbd90f5ac15b194b"
    
}

struct APPSCHEMA {
    static let FACEBOOK                 = "fb" + APPKEY.FACEBOOK_APPID
    static let INSTAGRAM                = "igAPPID://"
    static let PINTEREST                = "pdk" + APPKEY.PINTEREST_APPID
    static let TWITTER                  = "twitterkit-" + APPKEY.TWITTER_CONSUMERKEY
    static let SNAPCHAT                 = ""
}

struct USERDEFAULT_KEYNAME {
    static let INSTAGRAM_ACCESSTOKEN = "umo.instagram.accesstoken"
}

struct FIREBASE {
    static let BASEURL              = "https://umoapp-ba57e.firebaseio.com"
    static let STORAGE              = "gs://umoapp-ba57e.appspot.com"
}

struct SEGUE {
    static let GOTOMainTabVC            = "gotoMainTabVC"
    static let GOTOSocialLoginVC        = "gotoSocicalLoginVC"
    static let GOTOPurchaseDetailVC     = "gotoPurchaseDetailVC"
    static let GOTOConfirmPurchaseVC    = "gotoConfirmPurchaseVC"
    static let GOTOOutOfCreditsVC       = "gotoOutOfCreditsVC"
    static let GOTOUploadSelectResourceVC = "gotoUploadSelectResourceVC"
    static let GOTOFinalPostVC          = "gotoFinalPostVC"
    static let GOTOUploadPickerNC       = "gotoUploadPickerNC"
    static let GOTOInstagramLoginNC     = "gotoInstagramLoginNC"
    static let GOTOLoginNC              = "gotoLoginNC"
}

struct APPCOLOR {
    static let LIGHT_BLUE           = UIColor.init(red: 41/255.0, green: 180/255.0, blue: 231/255.0, alpha: 1)
    static let DARK_BLUE            = UIColor.init(red: 26/255.0, green: 83/255.0, blue: 94/255.0, alpha: 1)
    static let GRAY_BLUE            = UIColor.init(red: 184/255.0, green: 207/255.0, blue: 212/255.0, alpha: 1)
}

struct APPFONT {
    static let REGULAR              = "Montserrat-Regular"
    static let BOLD                 = "Montserrat-Bold"
    static let SEMIBOLD             = "Montserrat-SemiBold"
    static let LIGHT                = "Montserrat-Light"
    static let BLACK                = "Montserrat Black"
    static let EXTRABOLD            = "Montserrat-ExtraBold"
    static let HAIRLINE             = "Montserrat-Hairline"
    static let ULTRALIGHT           = "Montserrat-UltraLight"

}

struct TIMEFORMAT {
    static let DEFAULT              = "dd/MM/yyyy hh:mm:ss"
}

struct UPLOADTYPE {
    static let IMAGE                = "Image"
    static let VIDEO                = "Video"
    static let AUDIO                = "Audio"
    static let PDF                  = "PDF"
    static let NONE                 = "none"
}

struct UPLOADRESOURCE {
    static let LOCAL                = "local"
    static let DROPBOX              = "dropbox"
    static let BOX                  = "box"
    static let NONE                 = "none"
}

struct APPNOTIFICATION {
    static let POSTEDMEDIA          = "com.sbmllc.umo.Notification.PostedMedia"
    
}

// MARK: - Database

struct TABLENAME {
    static let USERINFO             = "userinfo"
    static let MEDIA                = "media"
    static let FOLLOWING            = "following"
    static let FAVORITE             = "favorite"
    static let LIBRARY              = "library"
}

struct FIELDNAME {
    /// User table
    static let USER_UID             = "user_uid"
    static let USER_USERNAME        = "username"
    static let USER_FULLNAME        = "fullname"
    static let USER_EMAIL           = "email"
    static let USER_ABOUT           = "about"
    static let USER_AVATAR          = "avatar"
    static let USER_INSTAGRAM       = "instagram"
    static let USER_TWITTER         = "twitter"
    static let USER_SNAPCHAT        = "snapchat"
    static let USER_PINTEREST       = "pinterest"
    static let USER_FACEBOOK        = "facebook"
    static let USER_DROPBOX         = "dropbox"
    static let USER_BOX             = "box"
    static let USER_FOLLOWINGS      = "followings"
    static let USER_FOLLOWERS       = "followers"
    static let USER_POSTS           = "posts"
    static let USER_FAVS            = "favs"
    static let USER_CREDITS         = "credits"
    static let USER_LIBRARIES       = "libraries"
    static let USER_PUBLISHER       = "publisher"
    
    /// Media table
    static let MEDIA_UID            = "media_uid"
    static let MEDIA_TYPE           = "type"
    static let MEDIA_FILE           = "file"
    static let MEDIA_THUMBNAIL      = "thumbnail"
    static let MEDIA_SOURCE         = "source"
    static let MEDIA_FILESIZE       = "filesize"
    static let MEDIA_DESCRIPTION    = "description"
    static let MEDIA_CREDIT         = "credit"
    static let MEDIA_CREATEDATE     = "create_date"
    static let MEDIA_UPDATEDATE     = "update_date"
    static let MEDIA_THUMBNAIL_DIMENTION      = "thumbnail_dimention"
    static let MEDIA_FAVS           = "favs"
    static let MEDIA_BUYERS         = "buyers"
    static let MEDIA_SOCIALS        = "socials"
    
    /// Following table
    static let FOLLOWING_USER       = "user"
    static let FOLLOWING_BYUSER     = "byuser"
    static let FOLLOWING_DATE       = "date"
    
    /// Favorite table
    static let FAVORITE_MEDIA       = "media"
    static let FAVORITE_USER        = "user"
    static let FAVORITE_DATE        = "date"
    
    /// Library table
    static let LIBRARY_MEDIA        = "media"
    static let LIBRARY_USER         = "user"
    static let LIBRARY_DATE         = "date"
}

struct STORAGEPATH {
    static let MEDIA                = "media"
    static let AVATAR               = "avatar"
    static let THUMBNAIL            = "thumbnail"
}

struct PLACEHOLDER {
    static func FEED(type: String!) -> UIImage! {
        var _placeholder: UIImage!
        switch type {
        case UPLOADTYPE.IMAGE:
            _placeholder = UIImage(named: "placeholderImage")!
            break
        case UPLOADTYPE.VIDEO:
            _placeholder = UIImage(named: "placeholderVideo")!
            break
        case UPLOADTYPE.AUDIO:
            _placeholder = UIImage(named: "placeholderAudio")!
            break
        case UPLOADTYPE.PDF:
            _placeholder = UIImage(named: "placeholderPDF")!
            break
        default:
            _placeholder = UIImage(named: "placeholderImage")!
            break
        }
        return _placeholder
    }
    static let AVATAR               = UIImage(named: "placeholderUser")!
}










