//
//  UMOAppManager.swift
//  UMO
//
//  Created by Boris on 6/29/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase

class UMOAppManager: NSObject {
    
    class var sharedManager: UMOAppManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: UMOAppManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = UMOAppManager()
        }
        return Static.instance!
    }
    
//    var fullUserInfo: [String: AnyObject]?
    var currentUser: UMOUserModel?
    
    var socialLoginSuccessInstagram: Bool = false
    var socialLoginSuccessTwitter: Bool = false
    var socialLoginSuccessPinterest: Bool = false
    var socialLoginSuccessSnapchat: Bool = false
    var socialLoginSuccessFacebook: Bool = false
    
    var uploadType = UPLOADTYPE.NONE
    var uploadResource = UPLOADRESOURCE.LOCAL
    var thumbnailResource = UPLOADRESOURCE.NONE
    
    override init() {
        super.init()
    }
}
