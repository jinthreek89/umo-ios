//
//  UMOCache.swift
//  UMO
//
//  Created by Boris on 7/20/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase

class UMOCache: NSObject {

    class var sharedManager: UMOCache {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: UMOCache? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = UMOCache()
        }
        return Static.instance!
    }

    private var _cacheForUser: [String: FIRDataSnapshot] = [:]
    private var _cacheForUserLoading: [String: Bool] = [:]
    
    func appManager() -> UMOAppManager {
        return UMOAppManager.sharedManager
    }
    
    func databaseRef() -> FIRDatabaseReference {
        return FIRDatabase.database().reference()
    }
    
    func storageRef() -> FIRStorageReference {
        return FIRStorage.storage().referenceForURL(FIREBASE.STORAGE)
    }
    
    func getUserInfo(key: String!) -> FIRDataSnapshot? {
        if let userinfo = _cacheForUser[key] {
            return userinfo
        } else {
            return nil
        }
    }
    
    func setUserInfo(key: String!, snapshot: FIRDataSnapshot) -> Void {
        _cacheForUser[key] = snapshot
    }
    
    func loadUserInfo(key: String!, success:((snapshot: FIRDataSnapshot?) -> Void)!) -> Void {
        if let userinfo = _cacheForUser[key] {
            success(snapshot: userinfo)
        } else {
            if (_cacheForUserLoading[key] != nil) {
                success(snapshot: _cacheForUser[key])
            } else {
                let loadComplete = { (snapshot: FIRDataSnapshot) in
                    if snapshot.childrenCount > 0 {
                        self._cacheForUser[key] = snapshot
                    }
                    success(snapshot: snapshot)
                }
                
                databaseRef().child(TABLENAME.USERINFO).child(key).observeSingleEventOfType(.Value, withBlock: loadComplete)
                
                databaseRef().child(TABLENAME.USERINFO).child(key).observeEventType(.ChildChanged, withBlock: loadComplete)
                
                _cacheForUserLoading[key] = true
            }
        }
    }
}
