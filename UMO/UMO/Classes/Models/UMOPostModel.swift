//
//  UMOPostModel.swift
//  UMO
//
//  Created by Boris on 7/18/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase

class UMOPostModel: UMOObject {
    
    var type:           String? = UPLOADTYPE.NONE
    var source:         String? = UPLOADRESOURCE.NONE
    var file:           String? = ""
    var thumbnail:      String? = ""
    var credits:        Int? = 1
    var userFullName:   String? = ""
    var userUID:        String? = ""
    var userAvatar:     String? = ""
    var userAbout:      String? = ""
    var favs:           [String]? = []
    var buyers:         [String]? = []
    var mediaDescription: String? = ""
    var socials:        [String]? = []
    var thumbnailDimension: CGSize? = CGSizeZero
    var fileSize:       Double? = 0
    var createDate:     NSDate? = NSDate()
    var updateDate:     NSDate? = NSDate()
    var userInfo: UMOUserModel?
    
    init(type: String!, source: String!, file: String!, thumbnail: String!, credits: Int!, userFullName: String!, userUID: String!, thumbnailDimention: CGSize!, createDate: NSDate!) {
        super.init()
        self.type               = type
        self.source             = source
        self.file               = file
        self.thumbnail          = thumbnail
        self.credits            = credits
        self.userFullName       = userFullName
        self.userUID            = userUID
        self.thumbnailDimension = thumbnailDimention
//        self.fileSize           = fileSize
        self.createDate         = createDate
    }
    
    init(uid: String!, type: String!, source: String!, file: String!, thumbnail: String!, credits: Int!, userFullName: String!, userUID: String!, thumbnailDimention: CGSize!, createDate: NSDate!) {
        super.init()
        self.UID                = uid
        self.type               = type
        self.source             = source
        self.file               = file
        self.thumbnail          = thumbnail
        self.credits            = credits
        self.userFullName       = userFullName
        self.userUID            = userUID
        self.thumbnailDimension = thumbnailDimention
//        self.fileSize           = fileSize
        self.createDate         = createDate
    }
    
    init(snapshot: FIRDataSnapshot) {
        super.init()
        self.UID = snapshot.key
        if let data = snapshot.valueInExportFormat() {
            self.type                   = data.objectForKey(FIELDNAME.MEDIA_TYPE) as? String
            self.source                 = data.objectForKey(FIELDNAME.MEDIA_SOURCE) as? String
            self.file                   = data.objectForKey(FIELDNAME.MEDIA_FILE) as? String
            self.thumbnail              = data.objectForKey(FIELDNAME.MEDIA_THUMBNAIL) as? String
            self.credits                = data.objectForKey(FIELDNAME.MEDIA_CREDIT) as? Int
            self.userFullName           = data.objectForKey(FIELDNAME.USER_FULLNAME) as? String
            self.userUID                = data.objectForKey(FIELDNAME.USER_UID) as? String
            self.userAvatar             = data.objectForKey(FIELDNAME.USER_AVATAR) as? String
            self.userAbout              = data.objectForKey(FIELDNAME.USER_ABOUT) as? String
            self.favs                   = data.objectForKey(FIELDNAME.MEDIA_FAVS) as? [String]
            self.buyers                 = data.objectForKey(FIELDNAME.MEDIA_BUYERS) as? [String]
            self.mediaDescription       = data.objectForKey(FIELDNAME.MEDIA_DESCRIPTION) as? String
            self.socials                = data.objectForKey(FIELDNAME.MEDIA_SOCIALS) as? [String]
            if let thumbnailDimension   = data.objectForKey(FIELDNAME.MEDIA_THUMBNAIL_DIMENTION) {
                self.thumbnailDimension = CGSizeFromString(thumbnailDimension as! String)
            } else {
                self.thumbnailDimension = CGSize(width: 0, height: 0)
            }
            if let fileSize             = data.objectForKey(FIELDNAME.MEDIA_FILESIZE) {
                self.fileSize           = fileSize as? Double
            }
            
            if let createDate = data.objectForKey(FIELDNAME.MEDIA_CREATEDATE) {
                self.createDate         = NSDate(timeIntervalSince1970: createDate as! Double)
            } else {
                self.createDate         = NSDate()
            }
            if let updateDate = data.objectForKey(FIELDNAME.MEDIA_UPDATEDATE) {
                self.updateDate         = NSDate(timeIntervalSince1970: updateDate as! Double)
            }
            
        }
    }
    
    override func getDictionaryData() -> [String : AnyObject!] {
        var result: [String: AnyObject!] = [:]
//            FIELDNAME.MEDIA_TYPE : self.type,
//            FIELDNAME.MEDIA_SOURCE : self.source,
//            FIELDNAME.MEDIA_FILE : self.file,
//            FIELDNAME.MEDIA_THUMBNAIL : self.thumbnail,
//            FIELDNAME.MEDIA_CREDIT : self.credits,
//            FIELDNAME.USER_FULLNAME : self.userFullName,
//            FIELDNAME.USER_UID : self.userUID,
//            FIELDNAME.MEDIA_THUMBNAIL_DIMENTION : NSStringFromCGSize(self.thumbnailDimension!),
//            FIELDNAME.MEDIA_CREATEDATE : self.createDate!.timeIntervalSince1970
//        ]
        if self.type != nil {
            result[FIELDNAME.MEDIA_TYPE] = self.type!
        }
        if self.source != nil {
            result[FIELDNAME.MEDIA_SOURCE] = self.source!
        }
        if self.file != nil {
            result[FIELDNAME.MEDIA_FILE] = self.file!
        }
        if self.thumbnail != nil {
            result[FIELDNAME.MEDIA_THUMBNAIL] = self.thumbnail!
        }
        if self.credits != nil {
            result[FIELDNAME.MEDIA_CREDIT] = self.credits!
        }
        if self.userFullName != nil {
            result[FIELDNAME.USER_FULLNAME] = self.userFullName!
        }
        if self.userUID != nil {
            result[FIELDNAME.USER_UID] = self.userUID!
        }
        if self.userAbout != nil {
            result[FIELDNAME.USER_ABOUT] = self.userAbout!
        }
        if self.thumbnailDimension != nil {
            result[FIELDNAME.MEDIA_THUMBNAIL_DIMENTION] = NSStringFromCGSize(self.thumbnailDimension!)
        }
        if self.createDate != nil {
            result[FIELDNAME.MEDIA_CREATEDATE] = self.createDate!.timeIntervalSince1970
        }
        if self.UID != nil {
            result[FIELDNAME.MEDIA_UID] = self.UID!
        }
        if self.userAvatar != nil {
            result[FIELDNAME.USER_AVATAR] = self.userAvatar!
        }
        if self.favs?.count > 0 {
            result[FIELDNAME.MEDIA_FAVS] = self.favs!
        }
        if self.buyers?.count > 0 {
            result[FIELDNAME.MEDIA_BUYERS] = self.buyers!
        }
        if self.fileSize != 0 {
            result[FIELDNAME.MEDIA_FILESIZE] = self.fileSize!
        }
        if self.mediaDescription != nil {
            result[FIELDNAME.MEDIA_DESCRIPTION] = self.mediaDescription!
        }
        if self.socials?.count > 0 {
            result[FIELDNAME.MEDIA_SOCIALS] = self.socials!
        }
        if self.updateDate != nil {
            result[FIELDNAME.MEDIA_UPDATEDATE] = self.updateDate!.timeIntervalSince1970
        }
        
        return result
    }
    
}
