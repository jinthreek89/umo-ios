//
//  UMOUserModel.swift
//  UMO
//
//  Created by Boris on 7/18/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase

class UMOUserModel: UMOObject {

    var email: String?
    var username: String?
    var fullname: String?
    var about: String?
    var avatar: String?
    var posts: [String]? = []
    var favs : [String]? = []
    var credits: Int?
    var facebook: String?
    var twitter: String?
    var pinterest: String?
    var instagram: String?
    var followings: [String]? = []
    var followers:  [String]? = []
    var libraries:  [String]? = []
    var dropbox: String?
    var box: String?
    var publisher: Int! = 1
    
    
    
    init(email: String!, username: String!, fullname: String!, credits: Int!) {
        super.init()
        self.email = email
        self.username = username
        self.fullname = fullname
        self.credits = credits
    }
    
    init(uid: String!, email: String!, username: String!, fullname: String!, credits: Int!) {
        super.init()
        self.UID = uid
        self.email = email
        self.username = username
        self.fullname = fullname
        self.credits = credits
    }
    
    init(snapshot: FIRDataSnapshot)
    {
        super.init()
        self.UID = snapshot.key
        if let data = snapshot.valueInExportFormat() {
            self.email      = data.objectForKey(FIELDNAME.USER_EMAIL) as? String
            self.username   = data.objectForKey(FIELDNAME.USER_USERNAME) as? String
            self.fullname   = data.objectForKey(FIELDNAME.USER_FULLNAME) as? String
            if let about    = data.objectForKey(FIELDNAME.USER_ABOUT) as? String {
                self.about  = about
            }
            if let avatar   = data.objectForKey(FIELDNAME.USER_AVATAR) as? String {
                self.avatar = avatar
            }
            if let posts    = data.objectForKey(FIELDNAME.USER_POSTS) {
//                for key: String in (posts as! NSDictionary).allKeys as! [String]{
//                    self.posts?.append((posts as! NSDictionary).objectForKey(key) as! String)
//                }
                self.posts  = parseWithArrayFrom(posts as! [String: AnyObject])
            }
            if let favs     = data.objectForKey(FIELDNAME.USER_FAVS) as? [String] {
                self.favs   = favs
            }
            self.credits    = data.objectForKey(FIELDNAME.USER_CREDITS) as? Int
            self.facebook   = data.objectForKey(FIELDNAME.USER_FACEBOOK) as? String
            self.twitter    = data.objectForKey(FIELDNAME.USER_TWITTER) as? String
            self.pinterest  = data.objectForKey(FIELDNAME.USER_PINTEREST) as? String
            self.instagram  = data.objectForKey(FIELDNAME.USER_INSTAGRAM) as? String
            self.followings = data.objectForKey(FIELDNAME.USER_FOLLOWINGS) as? [String]
            self.followers  = data.objectForKey(FIELDNAME.USER_FOLLOWERS) as? [String]
            self.libraries  = data.objectForKey(FIELDNAME.USER_LIBRARIES) as? [String]
            self.dropbox    = data.objectForKey(FIELDNAME.USER_DROPBOX) as? String
            self.box        = data.objectForKey(FIELDNAME.USER_BOX) as? String
            if let publisher = data.objectForKey(FIELDNAME.USER_PUBLISHER) {
                self.publisher = publisher as! Int
            }
        }
    }
    
    private func parseWithArrayFrom(dic: [String: AnyObject]) -> [String] {
        var result: [String] = []
        for key: String in dic.keys {
            result.append(dic[key] as! String)
        }
        return result
    }
    
    override func getDictionaryData() -> [String: AnyObject!] {
        
        var result : [String: AnyObject!] = [:]
        
        if self.email != nil {
            result[FIELDNAME.USER_EMAIL] = self.email!
        }
        if self.username != nil {
            result[FIELDNAME.USER_USERNAME] = self.username!
        }
        if self.fullname != nil {
            result[FIELDNAME.USER_FULLNAME] = self.fullname!
        }
        if self.credits != nil {
            result[FIELDNAME.USER_CREDITS] = self.credits!
        }
        if self.about != nil {
            result[FIELDNAME.USER_ABOUT] = self.about!
        }
        if self.avatar != nil {
            result[FIELDNAME.USER_AVATAR] = self.avatar!
        }
        if self.posts != nil { //?.count > 0 {
            result[FIELDNAME.USER_POSTS] = self.posts!
        }
        if self.favs != nil { //?.count > 0 {
            result[FIELDNAME.USER_FAVS]  = self.favs!
        }
        if self.facebook != nil {
            result[FIELDNAME.USER_FACEBOOK] = self.facebook!
        }
        if self.twitter != nil {
            result[FIELDNAME.USER_TWITTER]  = self.twitter!
        }
        if self.pinterest != nil {
            result[FIELDNAME.USER_PINTEREST] = self.pinterest!
        }
        if self.instagram != nil {
            result[FIELDNAME.USER_INSTAGRAM] = self.instagram!
        }
        if self.followers != nil { //?.count > 0 {
            result[FIELDNAME.USER_FOLLOWERS] = self.followers!
        }
        if self.followings != nil { //?.count > 0 {
            result[FIELDNAME.USER_FOLLOWINGS] = self.followings!
        }
        if self.libraries != nil { //?.count > 0 {
            result[FIELDNAME.USER_LIBRARIES] = self.libraries!
        }
        if self.dropbox != nil {
            result[FIELDNAME.USER_DROPBOX] = self.dropbox!
        }
        if self.box != nil {
            result[FIELDNAME.USER_BOX] = self.box!
        }
        result[FIELDNAME.USER_PUBLISHER] = self.publisher
        return result
    }
    
    func addPost(mediaUID: String!) -> Void {
        if self.UID == nil {
            return
        }
        
        if self.posts == nil {
            self.posts = []
        }
        self.posts?.append(mediaUID)
        _databaseRef.child(TABLENAME.USERINFO).child(self.UID!).updateChildValues([FIELDNAME.USER_POSTS : self.posts!])
    }
    
}
