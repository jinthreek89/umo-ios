//
//  UMOObject.swift
//  UMO
//
//  Created by Boris on 6/29/16.
//  Copyright © 2016 sbmllc. All rights reserved.
//

import UIKit
import Firebase

class UMOObject: NSObject {

    var UID: String?
    
    internal let _databaseRef = FIRDatabase.database().reference()
    
    func getDictionaryData() -> [String: AnyObject!] {
        return ["uid" : self.UID]
    }
}
